package com.app.haydaaapp.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.haydaaapp.R;
import com.app.haydaaapp.model.Subject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by abdullah on 5.9.2016.
 */
public class NtfAdapter extends RecyclerView.Adapter<NtfAdapter.MyViewHolder> {
    private List<Subject> subjectNtf;

    public NtfAdapter(List<Subject> subjectNtf) {
        this.subjectNtf = subjectNtf;
    }


    @Override
    public NtfAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.ntf_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        Date date = new Date(Long.parseLong(subjectNtf.get(position).getCreatedDate()));
        String dateText = new SimpleDateFormat("dd/MM/yyyy").format(date);
        holder.date.setText(dateText);
        holder.count.setText(subjectNtf.get(position).getComment_count());

    }


    @Override
    public int getItemCount() {
        return subjectNtf.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView date, count;

        public MyViewHolder(View itemView) {
            super(itemView);

            date = (TextView) itemView.findViewById(R.id.dateNtf);
            count = (TextView) itemView.findViewById(R.id.commentCountNtf);
        }
    }
}
