package com.app.haydaaapp.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.circleppgmailandroidlib.CircleImageView;
import com.app.haydaaapp.R;
import com.app.haydaaapp.model.BaseResponse;
import com.app.haydaaapp.model.Comment;
import com.app.haydaaapp.task.SetLikeCommentAsyncTask;
import com.app.haydaaapp.util.Utils;

import java.util.List;

/**
 * Created by abdullah sarıkaya on 28.8.2016.
 */
public class CommentListRecyclerViewAdapter extends RecyclerView.Adapter<CommentListRecyclerViewAdapter.MyViewHolder> implements SetLikeCommentAsyncTask.SetLikeCommentAsyncTaskCallBack {

    private List<Comment> comments;

    public CommentListRecyclerViewAdapter(List<Comment> comments) {
        this.comments = comments;
    }


    @Override
    public CommentListRecyclerViewAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = null;
        if (comments.size() > 0) {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.comment_list_item, parent, false);
            return new MyViewHolder(itemView);
        } else {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.default_comment_layout, parent, false);
        }

        return new MyViewHolder(itemView);
    }

    String likeCount = null;
    ImageView heartImage = null;
    TextView textLike = null;

    @Override
    public void onBindViewHolder(final CommentListRecyclerViewAdapter.MyViewHolder holder, final int position) {

        if (comments.size() > 0) {
            holder.userNameTv.setText(Utils.convertDecodeCharacters(comments.get(position).getUser().getUser_name()));
            holder.circleImageView.setUserName(Utils.convertDecodeCharacters(comments.get(position).getUser().getUser_name()));
            holder.circleImageView.setGender(Utils.convertDecodeCharacters(comments.get(position).getUser().getGender()));
            holder.universityNameTv.setText(Utils.convertDecodeCharacters(comments.get(position).getUser().getUniversity()));
            holder.departmentNameTv.setText(Utils.convertDecodeCharacters(comments.get(position).getUser().getDepartmant()));
            holder.commentText.setText(Utils.convertDecodeCharacters(comments.get(position).getText()));
            holder.likeCountTv.setText(Utils.convertDecodeCharacters(comments.get(position).getLikeCount()));


            holder.heart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Object[] params = new Object[2];
                    params[0] = holder.heart.getContext();
                    params[1] = comments.get(position).getId();
                    heartImage = holder.heart;
                    textLike = holder.likeCountTv;


                    if (!heartImage.isActivated()) {
                        new SetLikeCommentAsyncTask(CommentListRecyclerViewAdapter.this).execute(params);

                    } else {
                        Toast.makeText(heartImage.getContext(), "Yorumu zaten beğendiniz.", Toast.LENGTH_LONG).show();
                    }


                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return comments.size() > 0 ? comments.size() : 1;
    }

    @Override
    public void onSetLikeFinished(BaseResponse baseResponse) {
        if (baseResponse.getCode().equals("200")) {

            heartImage.setActivated(true);
            int a = Integer.parseInt(textLike.getText().toString()) + 1;
            textLike.setText(String.valueOf(a));


        } else {
            Toast.makeText(heartImage.getContext(), "Yorumu zaten beğendiniz.", Toast.LENGTH_LONG).show();
        }

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView userNameTv, universityNameTv, departmentNameTv, commentText, likeCountTv;
        public CircleImageView circleImageView;
        public ImageView heart;

        public MyViewHolder(View itemView) {
            super(itemView);
            if (comments.size() > 0) {
                userNameTv = (TextView) itemView.findViewById(R.id.commentUserNameId);
                universityNameTv = (TextView) itemView.findViewById(R.id.commentUniversityNameId);
                departmentNameTv = (TextView) itemView.findViewById(R.id.commentDepartmentNameId);
                commentText = (TextView) itemView.findViewById(R.id.commentTextId);
                likeCountTv = (TextView) itemView.findViewById(R.id.likeCountId);
                circleImageView = (CircleImageView) itemView.findViewById(R.id.commentCirclePP);
                circleImageView.setGender("Erkek");
                circleImageView.setUserName("...");
                heart = (ImageView) itemView.findViewById(R.id.heartId);
            }


        }
    }
}
