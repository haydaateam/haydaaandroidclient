package com.app.haydaaapp.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.circleppgmailandroidlib.CircleImageView;
import com.app.haydaaapp.R;
import com.app.haydaaapp.model.Subject;
import com.app.haydaaapp.util.Utils;

import java.util.List;

/**
 * Created by abdullah sarıkaya on 28.8.2016.
 */
public class SubjectListRecyclerViewAdapter extends RecyclerView.Adapter<SubjectListRecyclerViewAdapter.MyViewHolder> {

    private List<Subject> subjects;
    private int type;

    public SubjectListRecyclerViewAdapter(List<Subject> subjectList, int type) {
        this.subjects = subjectList;
        this.type = type;
    }


    @Override
    public SubjectListRecyclerViewAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = null;

        if (subjects.size() > 0) {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.subject_list_item, parent, false);
        } else {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.default_subject_layout, parent, false);
        }


        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(SubjectListRecyclerViewAdapter.MyViewHolder holder, int position) {
        if (subjects.size() > 0) {
            holder.userNameTv.setText(Utils.convertDecodeCharacters(subjects.get(position).getUser().getUser_name()));
            holder.circleImageView.setUserName(Utils.convertDecodeCharacters(subjects.get(position).getUser().getUser_name()));
            holder.circleImageView.setGender(Utils.convertDecodeCharacters(subjects.get(position).getUser().getGender()));
            holder.universityNameTv.setText(Utils.convertDecodeCharacters(subjects.get(position).getUser().getUniversity()));
            holder.departmentNameTv.setText(Utils.convertDecodeCharacters(subjects.get(position).getUser().getDepartmant()));
            holder.subjectText.setText(Utils.convertDecodeCharacters(subjects.get(position).getSubject_text()));
            holder.commentCountTv.setText(Utils.convertDecodeCharacters(subjects.get(position).getComment_count()));

            if (type == 2 || type == 3) {
                if (subjects.get(position).getIsRead() != null) {
                    if (subjects.get(position).getIsRead().equals("0")) {
                        holder.commentImage.setImageResource(R.mipmap.comment_dolu);
                    } else {
                        holder.commentImage.setImageResource(R.mipmap.comment_bos);
                    }
                }
            }


        }



    }

    @Override
    public int getItemCount() {
        return subjects.size() > 0 ? subjects.size() : 1;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView userNameTv, universityNameTv, departmentNameTv, subjectText, commentCountTv;
        public CircleImageView circleImageView;
        public ImageView commentImage;

        public MyViewHolder(View itemView) {
            super(itemView);

            userNameTv = (TextView) itemView.findViewById(R.id.userNameId);
            universityNameTv = (TextView) itemView.findViewById(R.id.universityNameId);
            departmentNameTv = (TextView) itemView.findViewById(R.id.departmentNameId);
            subjectText = (TextView) itemView.findViewById(R.id.subjectTextId);
            commentCountTv = (TextView) itemView.findViewById(R.id.commentCountId);
            circleImageView = (CircleImageView) itemView.findViewById(R.id.circlePP);
            commentImage = (ImageView) itemView.findViewById(R.id.commentImageId);

        }
    }
}
