package com.app.haydaaapp.main;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.app.circleppgmailandroidlib.CircleImageView;
import com.app.haydaaapp.R;
import com.app.haydaaapp.adapter.CommentListRecyclerViewAdapter;
import com.app.haydaaapp.model.BaseResponse;
import com.app.haydaaapp.model.Comment;
import com.app.haydaaapp.model.Subject;
import com.app.haydaaapp.task.GetCommentListBySubjectIdAsyncTask;
import com.app.haydaaapp.task.GetSubjectByIdAsyncTask;
import com.app.haydaaapp.util.Utils;

import java.util.ArrayList;
import java.util.List;

public class CommentActivity extends AppCompatActivity implements GetSubjectByIdAsyncTask.GetSubjectByIdAsyncTaskCallBack, GetCommentListBySubjectIdAsyncTask.GetCommentListBySubjectIdAsyncTaskCallBack, SwipeRefreshLayout.OnRefreshListener {

    private RecyclerView commentRecyclerView;
    private CommentListRecyclerViewAdapter adapter;
    private List<Comment> commentDataList = new ArrayList<>();

    SwipeRefreshLayout refreshLayout;

    private ProgressDialog progressDialog;
    String subjectId;

    TextView userName, universityName, departmentName, text;
    CircleImageView circleImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        View view = (View) findViewById(R.id.subjectItemId);
        userName = (TextView) view.findViewById(R.id.userNameId);
        universityName = (TextView) view.findViewById(R.id.universityNameId);
        departmentName = (TextView) view.findViewById(R.id.departmentNameId);
        text = (TextView) view.findViewById(R.id.subjectTextId);
        circleImageView = (CircleImageView) view.findViewById(R.id.circlePP);
        String gender = getIntent().getExtras().getString("gender", null);
        String userName = getIntent().getExtras().getString("userName", null);

        circleImageView.setUserName(userName);
        circleImageView.setGender(gender);

        refreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);


        subjectId = getIntent().getExtras().getString("subjectId");

        if (subjectId != null) {
            Object[] params = new Object[2];
            params[0] = CommentActivity.this;
            params[1] = subjectId;
            progressDialog = Utils.showProgress(CommentActivity.this, "", "Yükleniyor...");
            new GetSubjectByIdAsyncTask(CommentActivity.this).execute(params);
            new GetCommentListBySubjectIdAsyncTask(CommentActivity.this).execute(params);
        }


        commentRecyclerView = (RecyclerView) findViewById(R.id.commentList);

        refreshLayout.setOnRefreshListener(this);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putString("subjectId", subjectId);
                PopUpDialog popUpDialog = new PopUpDialog();
                popUpDialog.setArguments(bundle);
                popUpDialog.show(getFragmentManager(), "Hey");
            }
        });
    }

    @Override
    public void onGetSubjectByIdFinished(BaseResponse baseResponse) {
        if (baseResponse != null) {
            if (baseResponse.getCode().equals("200")) {
                Subject subject = (Subject) baseResponse.getResponseObject();
                userName.setText(Utils.convertDecodeCharacters(subject.getUser().getUser_name()));
                circleImageView.setGender(Utils.convertDecodeCharacters(subject.getUser().getGender()));
                circleImageView.setUserName(Utils.convertDecodeCharacters(subject.getUser().getUser_name()));
                universityName.setText(Utils.convertDecodeCharacters(subject.getUser().getUniversity()));
                departmentName.setText(Utils.convertDecodeCharacters(subject.getUser().getDepartmant()));
                text.setText(Utils.convertDecodeCharacters(subject.getSubject_text()));

            }
        } else {
            Utils.showAlertDialog(CommentActivity.this, "HATA", "Sistem hatası. Lütfen sonra tekrar deneyiniz!");
        }

    }

    @Override
    public void onGetCommentListFinished(BaseResponse baseResponse) {

        if (baseResponse != null) {
            if (baseResponse.getCode().equals("200")) {

                progressDialog.dismiss();
                commentDataList = (List<Comment>) baseResponse.getResponseObject();

                adapter = new CommentListRecyclerViewAdapter(commentDataList);

                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                commentRecyclerView.setLayoutManager(mLayoutManager);
                commentRecyclerView.setItemAnimator(new DefaultItemAnimator());
                commentRecyclerView.setAdapter(adapter);
                refreshLayout.setRefreshing(false);
            }

        } else {
            Utils.showAlertDialog(CommentActivity.this, "HATA", "Sistem hatası. Lütfen sonra tekrar deneyiniz!");
        }

    }

    @Override
    public void onRefresh() {
        if (subjectId != null) {
            Object[] params = new Object[2];
            params[0] = CommentActivity.this;
            params[1] = subjectId;
            new GetCommentListBySubjectIdAsyncTask(CommentActivity.this).execute(params);
        }
    }
}
