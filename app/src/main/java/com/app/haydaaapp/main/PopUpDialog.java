package com.app.haydaaapp.main;

import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.app.haydaaapp.R;
import com.app.haydaaapp.adapter.CommentListRecyclerViewAdapter;
import com.app.haydaaapp.model.BaseResponse;
import com.app.haydaaapp.model.Comment;
import com.app.haydaaapp.task.GetCommentListBySubjectIdAsyncTask;
import com.app.haydaaapp.task.SaveCommentAsyncTask;
import com.app.haydaaapp.util.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by metin2 on 28.8.2016.
 */
public class PopUpDialog extends DialogFragment implements SaveCommentAsyncTask.SaveCommentAsyncTaskCallBack, GetCommentListBySubjectIdAsyncTask.GetCommentListBySubjectIdAsyncTaskCallBack {

    private EditText comment;
    private Button send;
    ProgressDialog progressDialog;

    private RecyclerView commentRecyclerView;
    private CommentListRecyclerViewAdapter adapter;
    private List<Comment> commentDataList = new ArrayList<>();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.popup_fragment, container,
                false);
        // Do something else
        comment = (EditText) rootView.findViewById(R.id.editTextComment);
        send = (Button) rootView.findViewById(R.id.sendCommentId);

        commentRecyclerView = (RecyclerView) getActivity().findViewById(R.id.commentList);


        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = Utils.convertEncodeCharacters(comment.getText().toString());
                String subjectId = getArguments().getString("subjectId");
                if (comment.getText().toString().length() > 4) {
                    Object[] params = new Object[3];
                    params[0] = getActivity();
                    params[1] = text;
                    params[2] = subjectId;
                    progressDialog = Utils.showProgress(getActivity(), "", "Gönderiliyor...");
                    new SaveCommentAsyncTask(PopUpDialog.this).execute(params);
                } else {
                    Utils.showAlertDialog(getActivity(), "Uyarı", "Cevap verme daha iyi :) En az 5 karakter olmalıdır.");
                }

            }
        });
        return rootView;
    }

    @Override
    public void onSaveFinished(BaseResponse baseResponse) {
        if (baseResponse != null) {
            if (baseResponse.getCode().equals("200")) {
                progressDialog.dismiss();
                this.dismiss();

                String subjectId = getActivity().getIntent().getExtras().getString("subjectId", null);
                if (subjectId != null) {
                    Object[] params = new Object[2];
                    params[0] = getActivity();
                    params[1] = subjectId;
                    new GetCommentListBySubjectIdAsyncTask(PopUpDialog.this).execute(params);
                }


            }
        } else {
            Utils.showAlertDialog(getActivity(), "HATA", "Sistem hatası. Lütfen sonra tekrar deneyiniz!");
        }

    }


    @Override
    public void onGetCommentListFinished(BaseResponse baseResponse) {
        if (baseResponse.getCode().equals("200")) {

            commentDataList = (List<Comment>) baseResponse.getResponseObject();

            adapter = new CommentListRecyclerViewAdapter(commentDataList);

            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
            commentRecyclerView.setLayoutManager(mLayoutManager);
            commentRecyclerView.setItemAnimator(new DefaultItemAnimator());
            commentRecyclerView.setAdapter(adapter);
        }
    }
}
