package com.app.haydaaapp.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.app.haydaaapp.R;
import com.app.haydaaapp.adapter.NtfAdapter;
import com.app.haydaaapp.listener.RecyclerItemClickListener;
import com.app.haydaaapp.model.BaseResponse;
import com.app.haydaaapp.model.Subject;
import com.app.haydaaapp.task.GetSubjectNtfListByUserIdAsyncTask;

import java.util.ArrayList;
import java.util.List;

public class NtfSubjectActivity extends AppCompatActivity implements GetSubjectNtfListByUserIdAsyncTask.GetSubjectNtfListByUserIdAsyncTaskCallBack {

    RecyclerView recyclerView;
    private NtfAdapter adapter;
    private List<Subject> subjectList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ntf_subject);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerViewNtf);


        new GetSubjectNtfListByUserIdAsyncTask(NtfSubjectActivity.this).execute(NtfSubjectActivity.this);

        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(NtfSubjectActivity.this, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent i = new Intent(NtfSubjectActivity.this, CommentActivity.class);
                i.putExtra("subjectId", subjectList.get(position).getSubject_id());
                startActivity(i);
            }
        }));
    }

    @Override
    public void onGetSubjectNtfListFinished(BaseResponse baseResponse) {
        if (baseResponse.getCode().equals("200")) {
            subjectList = (List<Subject>) baseResponse.getResponseObject();

            adapter = new NtfAdapter(subjectList);

            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(adapter);
        }

    }
}
