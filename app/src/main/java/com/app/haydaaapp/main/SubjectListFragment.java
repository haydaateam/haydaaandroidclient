package com.app.haydaaapp.main;

import android.app.ProgressDialog;
import android.app.Service;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.app.haydaaapp.R;
import com.app.haydaaapp.adapter.SubjectListRecyclerViewAdapter;
import com.app.haydaaapp.listener.RecyclerItemClickListener;
import com.app.haydaaapp.model.BaseResponse;
import com.app.haydaaapp.model.Subject;
import com.app.haydaaapp.task.GetNewSubjectListAsyncTask;
import com.app.haydaaapp.task.GetPopularSubjectAsyncTask;
import com.app.haydaaapp.task.GetSubjectListByUserIdAsyncTask;
import com.app.haydaaapp.task.SaveSubjectAsyncTask;
import com.app.haydaaapp.util.Utils;

import java.util.ArrayList;
import java.util.List;


public class SubjectListFragment extends Fragment implements GetPopularSubjectAsyncTask.GetPopularSubjectAsyncTaskCallBack, GetNewSubjectListAsyncTask.GetNewSubjectListAsyncTaskCallBack, GetSubjectListByUserIdAsyncTask.GetSubjectListByUserIdAsyncTaskCallBack, SwipeRefreshLayout.OnRefreshListener, SaveSubjectAsyncTask.SaveSubjectAsyncTaskCallBack {

    private RecyclerView recyclerView;
    private SubjectListRecyclerViewAdapter adapter;
    private List<Subject> subjectList = new ArrayList<>();

    SwipeRefreshLayout refreshLayout;

    ViewPager viewPager;

    private ProgressDialog progressDialog;

    private View progressView;
    EditText subjectText;
    Button share;

    private ClipboardManager myClipboard;
    private ClipData myClip;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_subject_list, container, false);
        subjectList.clear();

        myClipboard = (ClipboardManager) getActivity().getSystemService(Service.CLIPBOARD_SERVICE);
        progressView = (ProgressBar) rootView.findViewById(R.id.progress_subject_list);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.subjectList);

        recyclerView.setVisibility(View.GONE);
        progressView.setVisibility(View.VISIBLE);


        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
        progressView.animate().setDuration(shortAnimTime);
        viewPager = (ViewPager) getActivity().findViewById(R.id.viewpager);

        refreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_refresh_layout);

        refreshLayout.setOnRefreshListener(this);

        new GetPopularSubjectAsyncTask(SubjectListFragment.this).execute(getActivity());

        subjectText = (EditText) getActivity().findViewById(R.id.newSubjectId);


        share = (Button) getActivity().findViewById(R.id.sendSubjectId);

        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (subjectText.getText().toString().length() > 5) {
                    if (subjectText.getText().toString().length() < 201) {
                        Object[] params = new Object[2];
                        params[0] = getActivity();
                        params[1] = Utils.convertEncodeCharacters(subjectText.getText().toString());
                        progressDialog = Utils.showProgress(getActivity(), "", "Gönderin paylaşılıyor...");
                        new SaveSubjectAsyncTask(SubjectListFragment.this).execute(params);
                    } else {
                        Utils.showAlertDialog(getActivity(), "Uyarı", "200 karakterden fazla yazamazsın.");
                    }
                } else {
                    Utils.showAlertDialog(getActivity(), "Uyarı", "Vur dedik öldürdün, çok az şey yazdın ?");
                }
                    }
        });


        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        new GetPopularSubjectAsyncTask(SubjectListFragment.this).execute(getActivity());
                        break;
                    case 1:
                        new GetNewSubjectListAsyncTask(SubjectListFragment.this).execute(getActivity());
                        break;
                    case 2:
                        new GetSubjectListByUserIdAsyncTask(SubjectListFragment.this).execute(getActivity());
                        break;

                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (!subjectList.isEmpty()) {
                    Intent i = new Intent(getActivity(), CommentActivity.class);
                    i.putExtra("subjectId", subjectList.get(position).getSubject_id());
                    i.putExtra("userName", subjectList.get(position).getUser().getUser_name());
                    i.putExtra("gender", subjectList.get(position).getUser().getGender());
                    startActivity(i);
                }

            }
        }));


        return rootView;
    }


    @Override
    public void onGetPopularSubjectListFinished(BaseResponse baseResponse) {
        if (baseResponse != null) {
            if (baseResponse.getCode().equals("200")) {
                progressView.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
                subjectList.clear();
                subjectList = (List<Subject>) baseResponse.getResponseObject();

                adapter = new SubjectListRecyclerViewAdapter(subjectList, 0);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
                recyclerView.setLayoutManager(mLayoutManager);
                recyclerView.setItemAnimator(new DefaultItemAnimator());
                recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                refreshLayout.setRefreshing(false);
            }

        } else {
            Utils.showAlertDialog(getActivity(), "HATA", "Sistem hatası. Lütfen sonra tekrar deneyiniz!");
        }

    }

    @Override
    public void onGetNewSubjectListFinished(BaseResponse baseResponse) {
        if (baseResponse != null) {
            if (baseResponse.getCode().equals("200")) {
                progressView.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
                subjectList.clear();
                subjectList = (List<Subject>) baseResponse.getResponseObject();

                adapter = new SubjectListRecyclerViewAdapter(subjectList, 1);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
                recyclerView.setLayoutManager(mLayoutManager);
                recyclerView.setItemAnimator(new DefaultItemAnimator());
                recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                refreshLayout.setRefreshing(false);
            }
        } else {
            Utils.showAlertDialog(getActivity(), "HATA", "Sistem hatası. Lütfen sonra tekrar deneyiniz!");
        }

    }

    @Override
    public void onGetSubjectListByUserIdFinished(BaseResponse baseResponse) {
        if (baseResponse != null) {
            if (baseResponse.getCode().equals("200")) {
                progressView.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
                subjectList.clear();
                subjectList = (List<Subject>) baseResponse.getResponseObject();

                adapter = new SubjectListRecyclerViewAdapter(subjectList, 2);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
                recyclerView.setLayoutManager(mLayoutManager);
                recyclerView.setItemAnimator(new DefaultItemAnimator());
                recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                refreshLayout.setRefreshing(false);
            }
        } else {
            Utils.showAlertDialog(getActivity(), "HATA", "Sistem hatası. Lütfen sonra tekrar deneyiniz!");
        }


    }

    @Override
    public void onRefresh() {

        switch (viewPager.getCurrentItem()) {
            case 0:
                new GetPopularSubjectAsyncTask(SubjectListFragment.this).execute(getActivity());
                break;
            case 1:
                new GetNewSubjectListAsyncTask(SubjectListFragment.this).execute(getActivity());
                break;
            case 2:
                new GetSubjectListByUserIdAsyncTask(SubjectListFragment.this).execute(getActivity());
                break;
        }

    }

    @Override
    public void onSaveSubjectFinished(BaseResponse baseResponse) {
        if (baseResponse != null) {
            if (baseResponse.getCode().equals("200")) {
                progressDialog.dismiss();
                subjectText.setText("");

                onRefresh();


            } else {
                Utils.showAlertDialog(getActivity(), "Uyarı", "Konunuz gönderilemedi.");
            }
        } else {
            Utils.showAlertDialog(getActivity(), "HATA", "Sistem hatası. Lütfen sonra tekrar deneyiniz!");
        }

    }
}
