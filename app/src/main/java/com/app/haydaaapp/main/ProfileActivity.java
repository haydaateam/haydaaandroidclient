package com.app.haydaaapp.main;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.app.circleppgmailandroidlib.CircleImageView;
import com.app.haydaaapp.R;
import com.app.haydaaapp.adapter.SubjectListRecyclerViewAdapter;
import com.app.haydaaapp.listener.RecyclerItemClickListener;
import com.app.haydaaapp.login.LoginActivity;
import com.app.haydaaapp.model.BaseResponse;
import com.app.haydaaapp.model.Subject;
import com.app.haydaaapp.model.User;
import com.app.haydaaapp.task.GetFollowingSubjectListByUserIdAsyncTask;
import com.app.haydaaapp.task.GetUserByIdAsyncTask;
import com.app.haydaaapp.util.AppSharedPrefs;
import com.app.haydaaapp.util.Utils;

import java.util.ArrayList;
import java.util.List;

public class ProfileActivity extends AppCompatActivity implements GetUserByIdAsyncTask.GetUserByIdAsyncTaskCallBack, GetFollowingSubjectListByUserIdAsyncTask.GetFollowingSubjectListByUserIdAsyncTaskCallBack {

    TextView commentCount, subjectCount, likeCount, userName;
    CircleImageView circleImageView;

    ProgressDialog progressDialog;
    Button bttnLogOut;

    private RecyclerView recyclerView;
    private SubjectListRecyclerViewAdapter adapter;
    private List<Subject> subjectList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        commentCount = (TextView) findViewById(R.id.commentCountPId);
        subjectCount = (TextView) findViewById(R.id.subjectCountPId);
        likeCount = (TextView) findViewById(R.id.likeCountPId);
        userName = (TextView) findViewById(R.id.userNamePID);


        recyclerView = (RecyclerView) findViewById(R.id.followedSubjectsId);

        circleImageView = (CircleImageView) findViewById(R.id.circlePPProfileId);

        progressDialog = Utils.showProgress(ProfileActivity.this, "", "Profil bilgileriniz yükleniyor..");
        Object[] params = new Object[1];
        params[0] = ProfileActivity.this;
        new GetUserByIdAsyncTask(ProfileActivity.this).execute(params);

        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(ProfileActivity.this, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent i = new Intent(ProfileActivity.this, CommentActivity.class);
                i.putExtra("subjectId", subjectList.get(position).getSubject_id());
                i.putExtra("userName", subjectList.get(position).getUser().getUser_name());
                i.putExtra("gender", subjectList.get(position).getUser().getGender());
                startActivity(i);
            }
        }));


    }

    @Override
    public void onGetUserFinished(BaseResponse baseResponse) {

        if (baseResponse.getCode().equals("200")) {
            User user = (User) baseResponse.getResponseObject();

            if (user.getCommentCount() == null) {
                commentCount.setText("0");
            } else {
                commentCount.setText(user.getCommentCount());
            }
            userName.setText(user.getUser_name());
            circleImageView.setUserName(user.getUser_name());
            circleImageView.setGender(user.getGender());

            if (user.getLikeCount() == null) {
                likeCount.setText("0");
            } else {
                likeCount.setText(user.getLikeCount());
            }

            if (user.getSubjectCount() == null) {
                subjectCount.setText("0");
            } else {
                subjectCount.setText(user.getSubjectCount());
            }

        } else {
            Utils.showAlertDialog(ProfileActivity.this, "Hata", "Sistemden kaynaklı bir hata oluştu.Sonra tekrar deneyin.");
        }
        progressDialog.dismiss();

        new GetFollowingSubjectListByUserIdAsyncTask(ProfileActivity.this).execute(ProfileActivity.this);
    }

    @Override
    public void onGetFollowingSubjectListFinished(BaseResponse baseResponse) {
        if (baseResponse.getCode().equals("200")) {
            subjectList.clear();
            subjectList = (List<Subject>) baseResponse.getResponseObject();

            adapter = new SubjectListRecyclerViewAdapter(subjectList, 3);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(adapter);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_profile, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.logoutId) {
            AppSharedPrefs.getInstance(getApplicationContext()).clearUser();
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                    Intent.FLAG_ACTIVITY_CLEAR_TASK
                    | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
