package com.app.haydaaapp.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.app.haydaaapp.R;
import com.app.haydaaapp.adapter.SubjectListRecyclerViewAdapter;
import com.app.haydaaapp.listener.RecyclerItemClickListener;
import com.app.haydaaapp.model.BaseResponse;
import com.app.haydaaapp.model.Department;
import com.app.haydaaapp.model.Subject;
import com.app.haydaaapp.model.University;
import com.app.haydaaapp.task.GetSubjectListByUniversityNameAsyncTask;
import com.app.haydaaapp.task.GetUniversityListAsyncTask;
import com.app.haydaaapp.util.Utils;

import java.util.ArrayList;
import java.util.List;

public class SearchActivity extends AppCompatActivity implements GetUniversityListAsyncTask.GetUniversityListAsyncTaskCallBack, GetSubjectListByUniversityNameAsyncTask.GetSubjectListByUniversityNameAsyncTaskCallBack {

    ListView listView;
    RecyclerView recyclerView;
    private SubjectListRecyclerViewAdapter adapter;
    private List<Subject> subjectList = new ArrayList<>();
    private List<University> universities = new ArrayList<>();
    private List<Department> departments = new ArrayList<>();

    String universityName = null;
    String departmentName = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        universityName = null;
        departmentName = null;

        listView = (ListView) findViewById(R.id.listviewCategory);
        recyclerView = (RecyclerView) findViewById(R.id.searchSubjectListId);


        new GetUniversityListAsyncTask(SearchActivity.this).execute(SearchActivity.this);


        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(SearchActivity.this, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (!subjectList.isEmpty()) {
                    Intent i = new Intent(SearchActivity.this, CommentActivity.class);
                    i.putExtra("subjectId", subjectList.get(position).getSubject_id());
                    i.putExtra("userName", subjectList.get(position).getUser().getUser_name());
                    i.putExtra("gender", subjectList.get(position).getUser().getGender());
                    startActivity(i);
                }

            }
        }));


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:

                if (listView.getItemAtPosition(listView.getCheckedItemPosition()) != null)
                    universityName = Utils.convertEncodeCharacters(listView.getItemAtPosition(listView.getCheckedItemPosition()).toString());
                if (universityName != null) {

                    Object[] params = new Object[2];
                    params[0] = SearchActivity.this;
                    params[1] = universityName;
                    new GetSubjectListByUniversityNameAsyncTask(SearchActivity.this).execute(params);


                } else {
                    Utils.showAlertDialog(SearchActivity.this, "Uyarı", "Lütfen üniversite seçin!");
                }

                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onGetUniversityListFinished(BaseResponse baseResponse) {
        if (baseResponse != null) {
            if (baseResponse.getCode().equals("200")) {
                universities = (List<University>) baseResponse.getResponseObject();
                final List<String> universityList = new ArrayList<>();
                for (University university : universities) {
                    universityList.add(university.getName());
                }
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_single_choice, universityList);

                listView.setAdapter(adapter);


            }
        } else {
            Utils.showAlertDialog(SearchActivity.this, "HATA", "Sistem hatası. Lütfen sonra tekrar deneyiniz!");
        }


    }


    @Override
    public void onGetSubjectListByUniversityFinished(BaseResponse baseResponse) {
        if (baseResponse != null) {
            if (baseResponse.getCode().equals("200")) {
                subjectList.clear();
                subjectList = (List<Subject>) baseResponse.getResponseObject();


                listView.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
                adapter = new SubjectListRecyclerViewAdapter(subjectList, 4);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                recyclerView.setLayoutManager(mLayoutManager);
                recyclerView.setItemAnimator(new DefaultItemAnimator());
                recyclerView.setAdapter(adapter);


                universityName = null;


                if (subjectList.isEmpty()) {
                    Utils.showAlertDialog(SearchActivity.this, "Uyarı", "Aradığınız konu bulunamadı.");
                }

            } else {
                Utils.showAlertDialog(SearchActivity.this, "Uyarı", "Aradığınız konu bulunamadı.");
            }

        } else {
            Utils.showAlertDialog(SearchActivity.this, "HATA", "Sistem hatası. Lütfen sonra tekrar deneyiniz!");
        }


    }
}
