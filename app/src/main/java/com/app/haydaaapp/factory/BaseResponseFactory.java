package com.app.haydaaapp.factory;


import com.app.haydaaapp.model.BaseResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

public class BaseResponseFactory {

    public static BaseResponse getBaseResponseFrom(String responseString) {

        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd' 'HH:mm:ss")
                .create();

        try {
            BaseResponse baseResponse = new BaseResponse();
            JSONObject jsonObject = new JSONObject(responseString);

            String code = jsonObject.get("responseCode").toString();
            String message = jsonObject.get("message").toString();
            String responseObjectType = jsonObject.get("responseType")
                    .toString();
            String responseObjectString = (String) jsonObject.get(
                    "responseObject").toString();

            baseResponse.setCode(code);
            baseResponse.setMessage(message);
            baseResponse.setResponseObjectType(responseObjectType);

            if (!responseObjectType.equals(null)
                    && !responseObjectType.equals("null")
                    && !responseObjectString.equals(null)
                    && !responseObjectString.equals("null")) {

                try {
                    Object responseObject = null;

                    Class<?> cls = Class.forName("com.app.haydaaapp.model."
                            + responseObjectType);
                    responseObject = gson.fromJson(responseObjectString, cls);

                    baseResponse.setResponseObject(responseObject);

                } catch (ClassNotFoundException e) {
                    try {
                        Object responseObject = null;
                        Class<?> cls = Class.forName("java.lang."
                                + responseObjectType);
                        responseObject = gson.fromJson(responseObjectString,
                                cls);
                        baseResponse.setResponseObject(responseObject);

                    } catch (ClassNotFoundException e2) {
                        e2.printStackTrace();
                    }
                }
            }

            return baseResponse;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}