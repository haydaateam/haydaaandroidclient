package com.app.haydaaapp.factory;


import com.app.haydaaapp.model.BaseResponse;
import com.app.haydaaapp.model.Department;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.List;

public class DepartmentBaseListResponseFactory {

    public static BaseResponse getBaseResponseFrom(String responseString) {

        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd' 'HH:mm:ss").create();

        try {
            BaseResponse baseResponse = new BaseResponse();
            JSONObject jsonObject = new JSONObject(responseString);

            String code = jsonObject.get("responseCode").toString();
            String message = jsonObject.get("message").toString();
            String responseObjectType = jsonObject.get("responseType").toString();
            String responseObjectString = (String) jsonObject.get("responseObject").toString();

            baseResponse.setCode(code);
            baseResponse.setMessage(message);
            baseResponse.setResponseObjectType(responseObjectType);

            try {
                Object responseObject = null;

                Type type = new TypeToken<List<Department>>() {
                }.getType();
                responseObject = gson.fromJson(responseObjectString, type);

                baseResponse.setResponseObject(responseObject);

            } catch (Exception e) {
                e.printStackTrace();
            }

            return baseResponse;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}