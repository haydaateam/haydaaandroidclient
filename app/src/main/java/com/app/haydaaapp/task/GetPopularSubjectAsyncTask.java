package com.app.haydaaapp.task;

import android.content.Context;
import android.os.AsyncTask;

import com.app.haydaaapp.model.BaseResponse;
import com.app.haydaaapp.service.SubjectService;
import com.app.haydaaapp.service.impl.SubjectServiceImpl;
import com.app.haydaaapp.util.AppSharedPrefs;

/**
 * Created by Abdullah Sarıkaya on 22.8.2016.
 */
public class GetPopularSubjectAsyncTask extends AsyncTask<Object, Void, BaseResponse> {

    public interface GetPopularSubjectAsyncTaskCallBack {
        public void onGetPopularSubjectListFinished(BaseResponse baseResponse);
    }

    public GetPopularSubjectAsyncTaskCallBack getPopularSubjectAsyncTaskCallBack;


    public GetPopularSubjectAsyncTask(GetPopularSubjectAsyncTaskCallBack getPopularSubjectAsyncTaskCallBack) {
        this.getPopularSubjectAsyncTaskCallBack = getPopularSubjectAsyncTaskCallBack;

    }

    @Override
    protected BaseResponse doInBackground(Object... params) {
        Context context = (Context) params[0];
        String userId = AppSharedPrefs.getInstance(context).getUserId();
        String regId = AppSharedPrefs.getInstance(context).getDeviceToken();

        SubjectService subjectService = new SubjectServiceImpl();
        BaseResponse baseResponse = subjectService.getPopularSubjectList(context, userId, regId);

        return baseResponse;
    }

    @Override
    protected void onPostExecute(BaseResponse baseResponse) {
        getPopularSubjectAsyncTaskCallBack.onGetPopularSubjectListFinished(baseResponse);
    }
}
