package com.app.haydaaapp.task;

import android.content.Context;
import android.os.AsyncTask;

import com.app.haydaaapp.model.BaseResponse;
import com.app.haydaaapp.service.SubjectService;
import com.app.haydaaapp.service.impl.SubjectServiceImpl;

/**
 * Created by Abdullah Sarıkaya on 22.8.2016.
 */
public class GetSubjectByIdAsyncTask extends AsyncTask<Object, Void, BaseResponse> {

    public interface GetSubjectByIdAsyncTaskCallBack {
        void onGetSubjectByIdFinished(BaseResponse baseResponse);
    }

    private GetSubjectByIdAsyncTaskCallBack getSubjectByIdAsyncTaskCallBack;


    public GetSubjectByIdAsyncTask(GetSubjectByIdAsyncTaskCallBack getSubjectByIdAsyncTaskCallBack) {
        this.getSubjectByIdAsyncTaskCallBack = getSubjectByIdAsyncTaskCallBack;

    }

    @Override
    protected BaseResponse doInBackground(Object... params) {
        Context context = (Context) params[0];
        String subjectId = (String) params[1];

        SubjectService subjectService = new SubjectServiceImpl();
        BaseResponse baseResponse = subjectService.getSubjectById(context, subjectId);

        return baseResponse;
    }

    @Override
    protected void onPostExecute(BaseResponse baseResponse) {
        getSubjectByIdAsyncTaskCallBack.onGetSubjectByIdFinished(baseResponse);
    }
}
