package com.app.haydaaapp.task;

import android.content.Context;
import android.os.AsyncTask;

import com.app.haydaaapp.model.BaseResponse;
import com.app.haydaaapp.service.UniversityAndDepartmentService;
import com.app.haydaaapp.service.impl.UniversityAndDepartmentServiceImpl;

/**
 * Created by Abdullah Sarıkaya on 22.8.2016.
 */
public class GetDepartmentListAsyncTask extends AsyncTask<Context, Void, BaseResponse> {

    public interface GetDepartmentListAsyncTaskCallBack {
        public void onGetDepartmentListFinished(BaseResponse baseResponse);
    }

    public GetDepartmentListAsyncTaskCallBack getDepartmentListAsyncTaskCallBack;


    public GetDepartmentListAsyncTask(GetDepartmentListAsyncTaskCallBack getDepartmentListAsyncTaskCallBack) {
        this.getDepartmentListAsyncTaskCallBack = getDepartmentListAsyncTaskCallBack;

    }

    @Override
    protected BaseResponse doInBackground(Context... params) {
        Context context = params[0];

        UniversityAndDepartmentService universityAndDepartmentService = new UniversityAndDepartmentServiceImpl();
        BaseResponse baseResponse = universityAndDepartmentService.getDepartmentList(context);

        return baseResponse;
    }

    @Override
    protected void onPostExecute(BaseResponse baseResponse) {
        getDepartmentListAsyncTaskCallBack.onGetDepartmentListFinished(baseResponse);
    }
}
