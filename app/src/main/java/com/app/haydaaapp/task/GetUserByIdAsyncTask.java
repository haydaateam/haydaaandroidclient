package com.app.haydaaapp.task;

import android.content.Context;
import android.os.AsyncTask;

import com.app.haydaaapp.model.BaseResponse;
import com.app.haydaaapp.service.UserService;
import com.app.haydaaapp.service.impl.UserServiceImpl;
import com.app.haydaaapp.util.AppSharedPrefs;

/**
 * Created by Abdullah Sarıkaya on 22.8.2016.
 */
public class GetUserByIdAsyncTask extends AsyncTask<Object, Void, BaseResponse> {

    public interface GetUserByIdAsyncTaskCallBack {
        void onGetUserFinished(BaseResponse baseResponse);
    }

    GetUserByIdAsyncTaskCallBack getUserByIdAsyncTaskCallBack;


    public GetUserByIdAsyncTask(GetUserByIdAsyncTaskCallBack getUserByIdAsyncTaskCallBack) {
        this.getUserByIdAsyncTaskCallBack = getUserByIdAsyncTaskCallBack;

    }

    @Override
    protected BaseResponse doInBackground(Object... params) {
        Context context = (Context) params[0];
        String userId = AppSharedPrefs.getInstance(context).getUserId();


        UserService userService = new UserServiceImpl();
        BaseResponse baseResponse = userService.getUserById(context, userId);

        return baseResponse;
    }

    @Override
    protected void onPostExecute(BaseResponse baseResponse) {
        getUserByIdAsyncTaskCallBack.onGetUserFinished(baseResponse);
    }
}
