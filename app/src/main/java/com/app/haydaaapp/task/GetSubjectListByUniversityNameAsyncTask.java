package com.app.haydaaapp.task;

import android.content.Context;
import android.os.AsyncTask;

import com.app.haydaaapp.model.BaseResponse;
import com.app.haydaaapp.service.SubjectService;
import com.app.haydaaapp.service.impl.SubjectServiceImpl;

/**
 * Created by Abdullah Sarıkaya on 22.8.2016.
 */
public class GetSubjectListByUniversityNameAsyncTask extends AsyncTask<Object, Void, BaseResponse> {

    public interface GetSubjectListByUniversityNameAsyncTaskCallBack {
        void onGetSubjectListByUniversityFinished(BaseResponse baseResponse);
    }

    private GetSubjectListByUniversityNameAsyncTaskCallBack getSubjectListByUniversityNameAsyncTaskCallBack;


    public GetSubjectListByUniversityNameAsyncTask(GetSubjectListByUniversityNameAsyncTaskCallBack getSubjectListByUniversityNameAsyncTaskCallBack) {
        this.getSubjectListByUniversityNameAsyncTaskCallBack = getSubjectListByUniversityNameAsyncTaskCallBack;

    }

    @Override
    protected BaseResponse doInBackground(Object... params) {
        Context context = (Context) params[0];
        String universityName = (String) params[1];


        SubjectService subjectService = new SubjectServiceImpl();
        BaseResponse baseResponse = subjectService.getSubjectListByUniversity(context, universityName);

        return baseResponse;
    }

    @Override
    protected void onPostExecute(BaseResponse baseResponse) {
        getSubjectListByUniversityNameAsyncTaskCallBack.onGetSubjectListByUniversityFinished(baseResponse);
    }
}
