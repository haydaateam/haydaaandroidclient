package com.app.haydaaapp.task;

import android.content.Context;
import android.os.AsyncTask;

import com.app.haydaaapp.model.BaseResponse;
import com.app.haydaaapp.service.UserService;
import com.app.haydaaapp.service.impl.UserServiceImpl;
import com.app.haydaaapp.util.AppSharedPrefs;

/**
 * Created by Abdullah Sarıkaya on 22.8.2016.
 */
public class SaveUserAsyncTask extends AsyncTask<Object, Void, BaseResponse> {

    public interface SaveUserAsyncTaskCallBack {
        void onSaveUserFinished(BaseResponse baseResponse);
    }

    private SaveUserAsyncTaskCallBack saveUserAsyncTaskCallBack;


    public SaveUserAsyncTask(SaveUserAsyncTaskCallBack getPopularSubjectAsyncTaskCallBack) {
        this.saveUserAsyncTaskCallBack = getPopularSubjectAsyncTaskCallBack;

    }

    @Override
    protected BaseResponse doInBackground(Object... params) {
        Context context = (Context) params[0];
        String userEmail = (String) params[1];
        String userName = (String) params[2];
        String userUniversity = (String) params[3];
        String userDepartment = (String) params[4];
        String userGender = (String) params[5];
        String userGradDate = (String) params[6];
        String userCode = (String) params[7];
        String deviceKey = AppSharedPrefs.getInstance(context).getDeviceToken();

        UserService userService = new UserServiceImpl();
        BaseResponse baseResponse = userService.newUser(context, userEmail, userName, userUniversity, userDepartment, userGender, userGradDate, userCode, deviceKey);

        return baseResponse;
    }

    @Override
    protected void onPostExecute(BaseResponse baseResponse) {
        saveUserAsyncTaskCallBack.onSaveUserFinished(baseResponse);
    }
}
