package com.app.haydaaapp.task;

import android.content.Context;
import android.os.AsyncTask;

import com.app.haydaaapp.model.BaseResponse;
import com.app.haydaaapp.service.CommentService;
import com.app.haydaaapp.service.impl.CommentServiceImpl;

/**
 * Created by Abdullah Sarıkaya on 22.8.2016.
 */
public class GetCommentListBySubjectIdAsyncTask extends AsyncTask<Object, Void, BaseResponse> {

    public interface GetCommentListBySubjectIdAsyncTaskCallBack {
        void onGetCommentListFinished(BaseResponse baseResponse);
    }

    private GetCommentListBySubjectIdAsyncTaskCallBack getCommentListBySubjectIdAsyncTaskCallBack;


    public GetCommentListBySubjectIdAsyncTask(GetCommentListBySubjectIdAsyncTaskCallBack getCommentListBySubjectIdAsyncTaskCallBack) {
        this.getCommentListBySubjectIdAsyncTaskCallBack = getCommentListBySubjectIdAsyncTaskCallBack;

    }

    @Override
    protected BaseResponse doInBackground(Object... params) {
        Context context = (Context) params[0];
        String subjectId = (String) params[1];

        CommentService commentService = new CommentServiceImpl();
        BaseResponse baseResponse = commentService.getCommentListById(context, subjectId);

        return baseResponse;
    }

    @Override
    protected void onPostExecute(BaseResponse baseResponse) {
        getCommentListBySubjectIdAsyncTaskCallBack.onGetCommentListFinished(baseResponse);
    }
}
