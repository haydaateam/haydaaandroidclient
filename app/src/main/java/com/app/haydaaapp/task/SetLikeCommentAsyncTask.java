package com.app.haydaaapp.task;

import android.content.Context;
import android.os.AsyncTask;

import com.app.haydaaapp.model.BaseResponse;
import com.app.haydaaapp.service.CommentService;
import com.app.haydaaapp.service.impl.CommentServiceImpl;
import com.app.haydaaapp.util.AppSharedPrefs;

/**
 * Created by Abdullah Sarıkaya on 22.8.2016.
 */
public class SetLikeCommentAsyncTask extends AsyncTask<Object, Void, BaseResponse> {

    public interface SetLikeCommentAsyncTaskCallBack {
        public void onSetLikeFinished(BaseResponse baseResponse);
    }

    private SetLikeCommentAsyncTaskCallBack setLikeCommentAsyncTaskCallBack;


    public SetLikeCommentAsyncTask(SetLikeCommentAsyncTaskCallBack setLikeCommentAsyncTaskCallBack) {
        this.setLikeCommentAsyncTaskCallBack = setLikeCommentAsyncTaskCallBack;

    }

    @Override
    protected BaseResponse doInBackground(Object... params) {
        Context context = (Context) params[0];
        String comment_id = (String) params[1];
        String userId = AppSharedPrefs.getInstance(context).getUserId();
        CommentService commentService = new CommentServiceImpl();
        BaseResponse baseResponse = commentService.setLikeComment(context, comment_id, userId);

        return baseResponse;
    }

    @Override
    protected void onPostExecute(BaseResponse baseResponse) {
        setLikeCommentAsyncTaskCallBack.onSetLikeFinished(baseResponse);
    }
}
