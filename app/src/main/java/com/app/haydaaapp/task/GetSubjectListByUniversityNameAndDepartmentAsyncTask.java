package com.app.haydaaapp.task;

import android.content.Context;
import android.os.AsyncTask;

import com.app.haydaaapp.model.BaseResponse;
import com.app.haydaaapp.service.SubjectService;
import com.app.haydaaapp.service.impl.SubjectServiceImpl;

/**
 * Created by Abdullah Sarıkaya on 22.8.2016.
 */
public class GetSubjectListByUniversityNameAndDepartmentAsyncTask extends AsyncTask<Object, Void, BaseResponse> {

    public interface GetSubjectListByUniversityNameAndDepartmentAsyncTaskCallBack {
        void onGetSubjectListByUniversityAndDepartmentFinished(BaseResponse baseResponse);
    }

    private GetSubjectListByUniversityNameAndDepartmentAsyncTaskCallBack getSubjectListByUniversityNameAndDepartmentAsyncTaskCallBack;


    public GetSubjectListByUniversityNameAndDepartmentAsyncTask(GetSubjectListByUniversityNameAndDepartmentAsyncTaskCallBack getSubjectListByUniversityNameAndDepartmentAsyncTaskCallBack) {
        this.getSubjectListByUniversityNameAndDepartmentAsyncTaskCallBack = getSubjectListByUniversityNameAndDepartmentAsyncTaskCallBack;

    }

    @Override
    protected BaseResponse doInBackground(Object... params) {
        Context context = (Context) params[0];
        String universityName = (String) params[1];
        String departmentName = (String) params[2];


        SubjectService subjectService = new SubjectServiceImpl();
        BaseResponse baseResponse = subjectService.getSubjectListByUniversityAndDepartment(context, universityName, departmentName);

        return baseResponse;
    }

    @Override
    protected void onPostExecute(BaseResponse baseResponse) {
        getSubjectListByUniversityNameAndDepartmentAsyncTaskCallBack.onGetSubjectListByUniversityAndDepartmentFinished(baseResponse);
    }
}
