package com.app.haydaaapp.task;

import android.content.Context;
import android.os.AsyncTask;

import com.app.haydaaapp.model.BaseResponse;
import com.app.haydaaapp.service.SubjectService;
import com.app.haydaaapp.service.impl.SubjectServiceImpl;
import com.app.haydaaapp.util.AppSharedPrefs;

/**
 * Created by Abdullah Sarıkaya on 22.8.2016.
 */
public class SaveSubjectAsyncTask extends AsyncTask<Object, Void, BaseResponse> {

    public interface SaveSubjectAsyncTaskCallBack {
        void onSaveSubjectFinished(BaseResponse baseResponse);
    }

    private SaveSubjectAsyncTaskCallBack saveSubjectAsyncTaskCallBack;


    public SaveSubjectAsyncTask(SaveSubjectAsyncTaskCallBack saveSubjectAsyncTaskCallBack) {
        this.saveSubjectAsyncTaskCallBack = saveSubjectAsyncTaskCallBack;

    }

    @Override
    protected BaseResponse doInBackground(Object... params) {
        Context context = (Context) params[0];
        String userId = AppSharedPrefs.getInstance(context).getUserId();
        String text = (String) params[1];

        BaseResponse baseResponse = null;

        SubjectService subjectService = new SubjectServiceImpl();
        if (userId != null) {
            baseResponse = subjectService.setNewSubject(context, userId, text);
        }


        return baseResponse;
    }

    @Override
    protected void onPostExecute(BaseResponse baseResponse) {
        saveSubjectAsyncTaskCallBack.onSaveSubjectFinished(baseResponse);
    }
}
