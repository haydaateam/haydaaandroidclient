package com.app.haydaaapp.task;

import android.content.Context;
import android.os.AsyncTask;

import com.app.haydaaapp.model.BaseResponse;
import com.app.haydaaapp.service.SubjectService;
import com.app.haydaaapp.service.impl.SubjectServiceImpl;
import com.app.haydaaapp.util.AppSharedPrefs;

/**
 * Created by Abdullah Sarıkaya on 22.8.2016.
 */
public class GetNewSubjectListAsyncTask extends AsyncTask<Object, Void, BaseResponse> {

    public interface GetNewSubjectListAsyncTaskCallBack {
        public void onGetNewSubjectListFinished(BaseResponse baseResponse);
    }

    public GetNewSubjectListAsyncTaskCallBack getNewSubjectListAsyncTaskCallBack;


    public GetNewSubjectListAsyncTask(GetNewSubjectListAsyncTaskCallBack getNewSubjectListAsyncTaskCallBack) {
        this.getNewSubjectListAsyncTaskCallBack = getNewSubjectListAsyncTaskCallBack;

    }

    @Override
    protected BaseResponse doInBackground(Object... params) {
        Context context = (Context) params[0];
        String userId = AppSharedPrefs.getInstance(context).getUserId();

        SubjectService subjectService = new SubjectServiceImpl();
        BaseResponse baseResponse = subjectService.getNewSubjectList(context, userId);

        return baseResponse;
    }

    @Override
    protected void onPostExecute(BaseResponse baseResponse) {
        getNewSubjectListAsyncTaskCallBack.onGetNewSubjectListFinished(baseResponse);
    }
}
