package com.app.haydaaapp.task;

import android.content.Context;
import android.os.AsyncTask;

import com.app.haydaaapp.model.BaseResponse;
import com.app.haydaaapp.service.UniversityAndDepartmentService;
import com.app.haydaaapp.service.impl.UniversityAndDepartmentServiceImpl;

/**
 * Created by Abdullah Sarıkaya on 22.8.2016.
 */
public class GetUniversityListAsyncTask extends AsyncTask<Context, Void, BaseResponse> {

    public interface GetUniversityListAsyncTaskCallBack {
        public void onGetUniversityListFinished(BaseResponse baseResponse);
    }

    public GetUniversityListAsyncTaskCallBack getUniversityListAsyncTaskCallBack;


    public GetUniversityListAsyncTask(GetUniversityListAsyncTaskCallBack getUniversityListAsyncTaskCallBack) {
        this.getUniversityListAsyncTaskCallBack = getUniversityListAsyncTaskCallBack;

    }

    @Override
    protected BaseResponse doInBackground(Context... params) {
        Context context = params[0];

        UniversityAndDepartmentService universityAndDepartmentService = new UniversityAndDepartmentServiceImpl();
        BaseResponse baseResponse = universityAndDepartmentService.getUniversityList(context);

        return baseResponse;
    }

    @Override
    protected void onPostExecute(BaseResponse baseResponse) {
        getUniversityListAsyncTaskCallBack.onGetUniversityListFinished(baseResponse);
    }
}
