package com.app.haydaaapp.task;

import android.content.Context;
import android.os.AsyncTask;

import com.app.haydaaapp.model.BaseResponse;
import com.app.haydaaapp.service.CommentService;
import com.app.haydaaapp.service.impl.CommentServiceImpl;
import com.app.haydaaapp.util.AppSharedPrefs;

/**
 * Created by Abdullah Sarıkaya on 22.8.2016.
 */
public class SaveCommentAsyncTask extends AsyncTask<Object, Void, BaseResponse> {

    public interface SaveCommentAsyncTaskCallBack {
        void onSaveFinished(BaseResponse baseResponse);
    }

    private SaveCommentAsyncTaskCallBack saveCommentAsyncTaskCallBack;


    public SaveCommentAsyncTask(SaveCommentAsyncTaskCallBack saveCommentAsyncTaskCallBack) {
        this.saveCommentAsyncTaskCallBack = saveCommentAsyncTaskCallBack;

    }

    @Override
    protected BaseResponse doInBackground(Object... params) {
        Context context = (Context) params[0];
        String userId = AppSharedPrefs.getInstance(context).getUserId();
        String text = (String) params[1];
        String subjectId = (String) params[2];

        CommentService commentService = new CommentServiceImpl();
        BaseResponse baseResponse = commentService.setNewComment(context, userId, text, subjectId);

        return baseResponse;
    }

    @Override
    protected void onPostExecute(BaseResponse baseResponse) {
        saveCommentAsyncTaskCallBack.onSaveFinished(baseResponse);
    }
}
