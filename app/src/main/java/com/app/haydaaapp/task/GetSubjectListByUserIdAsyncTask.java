package com.app.haydaaapp.task;

import android.content.Context;
import android.os.AsyncTask;

import com.app.haydaaapp.model.BaseResponse;
import com.app.haydaaapp.service.SubjectService;
import com.app.haydaaapp.service.impl.SubjectServiceImpl;
import com.app.haydaaapp.util.AppSharedPrefs;

/**
 * Created by Abdullah Sarıkaya on 22.8.2016.
 */
public class GetSubjectListByUserIdAsyncTask extends AsyncTask<Context, Void, BaseResponse> {

    public interface GetSubjectListByUserIdAsyncTaskCallBack {
        void onGetSubjectListByUserIdFinished(BaseResponse baseResponse);
    }

    public GetSubjectListByUserIdAsyncTaskCallBack getSubjectListByUserIdAsyncTaskCallBack;


    public GetSubjectListByUserIdAsyncTask(GetSubjectListByUserIdAsyncTaskCallBack getSubjectListByUserIdAsyncTaskCallBack) {
        this.getSubjectListByUserIdAsyncTaskCallBack = getSubjectListByUserIdAsyncTaskCallBack;

    }

    @Override
    protected BaseResponse doInBackground(Context... params) {
        Context context = params[0];


        SubjectService subjectService = new SubjectServiceImpl();
        BaseResponse baseResponse = subjectService.getSubjectListByUserId(context, AppSharedPrefs.getInstance(context).getUserId());

        return baseResponse;
    }

    @Override
    protected void onPostExecute(BaseResponse baseResponse) {
        getSubjectListByUserIdAsyncTaskCallBack.onGetSubjectListByUserIdFinished(baseResponse);
    }
}
