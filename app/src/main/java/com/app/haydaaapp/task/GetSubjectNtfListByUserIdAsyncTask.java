package com.app.haydaaapp.task;

import android.content.Context;
import android.os.AsyncTask;

import com.app.haydaaapp.model.BaseResponse;
import com.app.haydaaapp.service.SubjectService;
import com.app.haydaaapp.service.impl.SubjectServiceImpl;
import com.app.haydaaapp.util.AppSharedPrefs;

/**
 * Created by Abdullah Sarıkaya on 22.8.2016.
 */
public class GetSubjectNtfListByUserIdAsyncTask extends AsyncTask<Context, Void, BaseResponse> {

    public interface GetSubjectNtfListByUserIdAsyncTaskCallBack {
        void onGetSubjectNtfListFinished(BaseResponse baseResponse);
    }

    private GetSubjectNtfListByUserIdAsyncTaskCallBack getSubjectNtfListByUserIdAsyncTaskCallBack;


    public GetSubjectNtfListByUserIdAsyncTask(GetSubjectNtfListByUserIdAsyncTaskCallBack getSubjectNtfListByUserIdAsyncTaskCallBack) {
        this.getSubjectNtfListByUserIdAsyncTaskCallBack = getSubjectNtfListByUserIdAsyncTaskCallBack;

    }

    @Override
    protected BaseResponse doInBackground(Context... params) {
        Context context = (Context) params[0];
        String userId = (String) AppSharedPrefs.getInstance(context).getUserId();


        SubjectService subjectService = new SubjectServiceImpl();
        BaseResponse baseResponse = subjectService.getSubjectNtfList(context, userId);

        return baseResponse;
    }

    @Override
    protected void onPostExecute(BaseResponse baseResponse) {
        getSubjectNtfListByUserIdAsyncTaskCallBack.onGetSubjectNtfListFinished(baseResponse);
    }
}
