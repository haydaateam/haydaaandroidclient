package com.app.haydaaapp.task;

import android.content.Context;
import android.os.AsyncTask;

import com.app.haydaaapp.model.BaseResponse;
import com.app.haydaaapp.service.UserService;
import com.app.haydaaapp.service.impl.UserServiceImpl;

/**
 * Created by Abdullah Sarıkaya on 22.8.2016.
 */
public class GetUserUserCodeAndPasswordAsyncTask extends AsyncTask<Object, Void, BaseResponse> {

    public interface GetUserUserCodeAndPasswordAsyncTaskCallBack {
        void onGetUserFinished(BaseResponse baseResponse);
    }

    GetUserUserCodeAndPasswordAsyncTaskCallBack getUserUserCodeAndPasswordAsyncTaskCallBack;


    public GetUserUserCodeAndPasswordAsyncTask(GetUserUserCodeAndPasswordAsyncTaskCallBack getUserUserCodeAndPasswordAsyncTaskCallBack) {
        this.getUserUserCodeAndPasswordAsyncTaskCallBack = getUserUserCodeAndPasswordAsyncTaskCallBack;

    }

    @Override
    protected BaseResponse doInBackground(Object... params) {
        Context context = (Context) params[0];
        String userCode = (String) params[1];
        String password = (String) params[2];

        UserService userService = new UserServiceImpl();
        BaseResponse baseResponse = userService.getUserByUserNameAndCode(context, userCode, password);

        return baseResponse;
    }

    @Override
    protected void onPostExecute(BaseResponse baseResponse) {
        getUserUserCodeAndPasswordAsyncTaskCallBack.onGetUserFinished(baseResponse);
    }
}
