package com.app.haydaaapp.task;

import android.content.Context;
import android.os.AsyncTask;

import com.app.haydaaapp.model.BaseResponse;
import com.app.haydaaapp.service.CommentService;
import com.app.haydaaapp.service.impl.CommentServiceImpl;
import com.app.haydaaapp.util.AppSharedPrefs;

/**
 * Created by Abdullah Sarıkaya on 22.8.2016.
 */
public class GetFollowingSubjectListByUserIdAsyncTask extends AsyncTask<Context, Void, BaseResponse> {

    public interface GetFollowingSubjectListByUserIdAsyncTaskCallBack {
        void onGetFollowingSubjectListFinished(BaseResponse baseResponse);
    }

    private GetFollowingSubjectListByUserIdAsyncTaskCallBack getFollowingSubjectListByUserIdAsyncTaskCallBack;


    public GetFollowingSubjectListByUserIdAsyncTask(GetFollowingSubjectListByUserIdAsyncTaskCallBack getFollowingSubjectListByUserIdAsyncTaskCallBack) {
        this.getFollowingSubjectListByUserIdAsyncTaskCallBack = getFollowingSubjectListByUserIdAsyncTaskCallBack;

    }

    @Override
    protected BaseResponse doInBackground(Context... params) {
        Context context = (Context) params[0];
        String userId = AppSharedPrefs.getInstance(context).getUserId();

        CommentService commentService = new CommentServiceImpl();
        BaseResponse baseResponse = commentService.getFollowingSubjectListByUserId(context, userId);

        return baseResponse;
    }

    @Override
    protected void onPostExecute(BaseResponse baseResponse) {
        getFollowingSubjectListByUserIdAsyncTaskCallBack.onGetFollowingSubjectListFinished(baseResponse);
    }
}
