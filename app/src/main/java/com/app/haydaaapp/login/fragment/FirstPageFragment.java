package com.app.haydaaapp.login.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.app.haydaaapp.R;
import com.app.haydaaapp.util.Utils;

public class FirstPageFragment extends Fragment {

    EditText userName, email, password, passwordAgain;
    Button next;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_first_page, container, false);

        userName = (EditText) rootView.findViewById(R.id.signupUserNameId);
        email = (EditText) rootView.findViewById(R.id.signupEmailId);
        password = (EditText) rootView.findViewById(R.id.signupCodeId);
        passwordAgain = (EditText) rootView.findViewById(R.id.signupCodeAgainId);

        next = (Button) rootView.findViewById(R.id.buttonFirstContinueId);

        isValid(userName, email, password, passwordAgain, next);

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isValidEmail(email.getText().toString())) {
                    if (userName.getText().toString().length() > 2 && userName.getText().toString().length() < 17 && password.getText().toString().length() > 3) {

                        Bundle bundle = new Bundle();
                        bundle.putString("userName", Utils.convertEncodeCharacters(userName.getText().toString().trim()));
                        bundle.putString("email", Utils.convertEncodeCharacters(email.getText().toString().trim()));
                        bundle.putString("userCode", password.getText().toString().trim());

                        SecondPageFragment secondPageFragment = new SecondPageFragment();
                        secondPageFragment.setArguments(bundle);
                        getFragmentManager().beginTransaction().replace(R.id.frame, secondPageFragment).commit();
                    } else {
                        Utils.showAlertDialog(getActivity(), "Uyarı", "Kullanıcı adı en az 3 en fazla 16, parola ise en az 4 karakter olmalıdır.");
                    }
                } else {
                    Utils.showAlertDialog(getActivity(), "Uyarı", "Girdiğiniz e-mail yanlıştır.");
                }

            }
        });

        return rootView;
    }

    public void isValid(final EditText userName, final EditText email, final EditText password, final EditText passwordAgain, final Button button) {


        passwordAgain.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (userName != null && email != null && password != null &&
                        password.getText().toString().equals(passwordAgain.getText().toString())) {
                    button.setVisibility(View.VISIBLE);
                }
            }
        });

    }


}
