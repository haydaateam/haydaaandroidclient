package com.app.haydaaapp.login.fragment;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;

import com.app.haydaaapp.R;
import com.app.haydaaapp.model.BaseResponse;
import com.app.haydaaapp.model.Department;
import com.app.haydaaapp.model.University;
import com.app.haydaaapp.task.GetDepartmentListAsyncTask;
import com.app.haydaaapp.task.GetUniversityListAsyncTask;
import com.app.haydaaapp.task.SaveUserAsyncTask;
import com.app.haydaaapp.util.Utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;


public class SecondPageFragment extends Fragment implements AdapterView.OnItemSelectedListener, GetUniversityListAsyncTask.GetUniversityListAsyncTaskCallBack, GetDepartmentListAsyncTask.GetDepartmentListAsyncTaskCallBack, SaveUserAsyncTask.SaveUserAsyncTaskCallBack {

    String gender = null;
    String universityName = null;
    String departmentName = null;
    String gradDate = null;
    String userName = null;
    String email = null;
    String userCode = null;

    List<University> universityList = new ArrayList();
    List<Department> departmentList = new ArrayList();
    EditText date;

    private DatePickerDialog toDatePickerDialog;

    private SimpleDateFormat dateFormatter;

    Spinner spinnerUniversity, spinnerDepartment;

    private ProgressDialog progressDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_second_page, container, false);
        Spinner spinnerGender = (Spinner) rootView.findViewById(R.id.genderSpinnerId);
        spinnerUniversity = (Spinner) rootView.findViewById(R.id.universitySpinnerId);
        spinnerDepartment = (Spinner) rootView.findViewById(R.id.departmentSpinnerId);
        date = (EditText) rootView.findViewById(R.id.datePickerId);
        final Button okBttn = (Button) rootView.findViewById(R.id.buttonSecondContinueId);
        date.setInputType(InputType.TYPE_NULL);
        date.requestFocus();

        Bundle bundle = getArguments();
        userName = bundle.getString("userName", null);
        email = bundle.getString("email", null);
        userCode = bundle.getString("userCode", null);

        okBttn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (userName != null || userCode != null || email != null || universityName != null || departmentName != null) {

                    final AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();

                    alertDialog.setTitle("Emin misin?");
                    alertDialog.setMessage("Doğru girdiğine emin ol. Çünkü tekrar değiştiremezsin!");

                    alertDialog.setButton(DialogInterface.BUTTON_POSITIVE,
                            "Eminim",
                            new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    gradDate = date.getText().toString().trim();

                                    Object[] params = new Object[8];
                                    params[0] = getActivity();
                                    params[1] = email;
                                    params[2] = userName;
                                    params[3] = universityName;
                                    params[4] = departmentName;
                                    params[5] = gender;
                                    params[6] = gradDate;
                                    params[7] = userCode;
                                    progressDialog = Utils.showProgress(getActivity(), "Kayıt ekleniyor...", "Lütfen Bekleyin");
                                    new SaveUserAsyncTask(SecondPageFragment.this).execute(params);


                                }
                            });
                    alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Emin değilim",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    alertDialog.dismiss();
                                }
                            });

                    alertDialog.show();


                } else {
                    Utils.showAlertDialog(getActivity(), "Uyarı!", "İşlem gerçekleştirilemiyor. Boş alan bıraktınız.");
                }

            }
        });
        dateFormatter = new SimpleDateFormat("dd/MM/yyyy", Locale.US);

        Calendar newCalendar = Calendar.getInstance();
        toDatePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                date.setText(dateFormatter.format(newDate.getTime()));
                okBttn.setVisibility(View.VISIBLE);
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toDatePickerDialog.show();
            }
        });


        spinnerGender.setOnItemSelectedListener(SecondPageFragment.this);
        spinnerUniversity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                universityName = Utils.convertEncodeCharacters(parent.getItemAtPosition(position).toString());
                Utils.LogD(universityName);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinnerDepartment.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                departmentName = Utils.convertEncodeCharacters(parent.getItemAtPosition(position).toString());
                Utils.LogD(departmentName);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        List<String> genderList = new ArrayList<>();
        genderList.add("Erkek");
        genderList.add("Kız");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, genderList);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerGender.setAdapter(dataAdapter);


        new GetUniversityListAsyncTask(SecondPageFragment.this).execute(getActivity());
        new GetDepartmentListAsyncTask(SecondPageFragment.this).execute(getActivity());

        return rootView;
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        gender = Utils.convertEncodeCharacters(parent.getItemAtPosition(position).toString());
        Utils.LogD(gender);


    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onGetUniversityListFinished(BaseResponse baseResponse) {
        if (baseResponse != null) {
            if (baseResponse.getCode().equals("200")) {
                universityList = (List<University>) baseResponse.getResponseObject();
                List<String> list = new ArrayList<>();
                for (University university : universityList) {
                    list.add(university.getName());
                }
                ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, list);
                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinnerUniversity.setAdapter(dataAdapter);

            }
        } else {
            Utils.showAlertDialog(getActivity(), "HATA", "Şuanda servisimiz hizmet vermemektedir.Daha sonra tekrar deneyin.");
        }

    }

    @Override
    public void onGetDepartmentListFinished(BaseResponse baseResponse) {
        if (baseResponse != null) {
            if (baseResponse.getCode().equals("200")) {
                departmentList = (List<Department>) baseResponse.getResponseObject();
                List<String> list = new ArrayList<>();
                for (Department department : departmentList) {
                    list.add(department.getName());
                }
                ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, list);
                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinnerDepartment.setAdapter(dataAdapter);


            }
        } else {
            Utils.showAlertDialog(getActivity(), "HATA", "Şuanda servisimiz hizmet vermemektedir. Daha sonra tekrar deneyin.");
        }

    }

    @Override
    public void onSaveUserFinished(BaseResponse baseResponse) {
        progressDialog.dismiss();
        if (baseResponse != null) {
            if (baseResponse.getCode().equals("200")) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setMessage(baseResponse.getMessage())
                        .setPositiveButton("GİRİŞ YAP", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                getActivity().finish();
                            }
                        });
                builder.show();

            } else {
                Utils.showAlertDialog(getActivity(), "Başarısız", "Sistem Hatası! Daha sonra tekrar deneyin.");
            }
        } else {
            Utils.showAlertDialog(getActivity(), "HATA", "Şuanda servisimiz hizmet vermemektedir. Daha sonra tekrar deneyin.");
        }

    }
}
