package com.app.haydaaapp.login;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.app.haydaaapp.R;
import com.app.haydaaapp.login.fragment.FirstPageFragment;

public class SignUpActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        getSupportFragmentManager().beginTransaction().add(R.id.frame, new FirstPageFragment()).commit();
    }
}
