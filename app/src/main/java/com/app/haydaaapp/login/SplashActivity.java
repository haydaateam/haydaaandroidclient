package com.app.haydaaapp.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.app.haydaaapp.R;
import com.app.haydaaapp.main.MainActivity;
import com.app.haydaaapp.util.AppSharedPrefs;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        decideMainScreen();

    }

    public void decideMainScreen() {

        if (AppSharedPrefs.getInstance(SplashActivity.this).getUserId() == null && AppSharedPrefs.getInstance(SplashActivity.this).getShowTutorial()) {
            startActivity(new Intent(SplashActivity.this, LoginActivity.class));
        } else if (AppSharedPrefs.getInstance(SplashActivity.this).getUserId() == null && !AppSharedPrefs.getInstance(SplashActivity.this).getShowTutorial()) {
            startActivity(new Intent(SplashActivity.this, TutorialActivity.class));
        } else if (AppSharedPrefs.getInstance(SplashActivity.this).getUserId() != null) {
            startActivity(new Intent(SplashActivity.this, MainActivity.class));
        }
        finish();
    }
}










