package com.app.haydaaapp.service;

import android.content.Context;

import com.app.haydaaapp.model.BaseResponse;

/**
 * Created by abdullah sarıkaya on 29.8.2016.
 */
public interface UniversityAndDepartmentService {

    public BaseResponse getUniversityList(Context ctx);

    public BaseResponse getDepartmentList(Context ctx);

}
