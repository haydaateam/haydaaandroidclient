package com.app.haydaaapp.service;

import android.content.Context;

import com.app.haydaaapp.model.BaseResponse;

/**
 * Created by abdullah sarıkaya on 26.8.2016.
 */
public interface CommentService {

    public BaseResponse getCommentListById(Context ctx, String subjectId);

    public BaseResponse getFollowingSubjectListByUserId(Context ctx, String userId);

    public BaseResponse setNewComment(Context ctx, String userId, String text, String subjectId);

    public BaseResponse setLikeComment(Context ctx, String commentId, String userId);

}
