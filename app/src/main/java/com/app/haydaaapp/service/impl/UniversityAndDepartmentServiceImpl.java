package com.app.haydaaapp.service.impl;

import android.content.Context;

import com.app.haydaaapp.dao.UniversityAndDepartmentDao;
import com.app.haydaaapp.dao.impl.UniversityAndDepartmentDaoImpl;
import com.app.haydaaapp.factory.DepartmentBaseListResponseFactory;
import com.app.haydaaapp.factory.UniversityBaseListResponseFactory;
import com.app.haydaaapp.model.BaseResponse;
import com.app.haydaaapp.service.UniversityAndDepartmentService;

/**
 * Created by abdullah sarıkaya on 29.8.2016.
 */
public class UniversityAndDepartmentServiceImpl implements UniversityAndDepartmentService {
    private UniversityAndDepartmentDao universityAndDepartmentDao;

    public UniversityAndDepartmentServiceImpl() {
        this.universityAndDepartmentDao = new UniversityAndDepartmentDaoImpl();
    }

    @Override
    public BaseResponse getUniversityList(Context ctx) {
        BaseResponse baseResponse = UniversityBaseListResponseFactory.getBaseResponseFrom(universityAndDepartmentDao.getUniversityList(ctx));
        return baseResponse;
    }

    @Override
    public BaseResponse getDepartmentList(Context ctx) {
        BaseResponse baseResponse = DepartmentBaseListResponseFactory.getBaseResponseFrom(universityAndDepartmentDao.getDepartmentList(ctx));
        return baseResponse;
    }
}
