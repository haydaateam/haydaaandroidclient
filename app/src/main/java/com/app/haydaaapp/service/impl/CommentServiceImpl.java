package com.app.haydaaapp.service.impl;

import android.content.Context;

import com.app.haydaaapp.dao.CommentDao;
import com.app.haydaaapp.dao.impl.CommentDaoImpl;
import com.app.haydaaapp.factory.BaseResponseFactory;
import com.app.haydaaapp.factory.CommentBaseListResponseFactory;
import com.app.haydaaapp.factory.SubjectBaseListResponseFactory;
import com.app.haydaaapp.model.BaseResponse;
import com.app.haydaaapp.service.CommentService;

/**
 * Created by abdullah sarıkaya on 26.8.2016.
 */
public class CommentServiceImpl implements CommentService {

    private CommentDao commentDao;

    public CommentServiceImpl() {
        this.commentDao = new CommentDaoImpl();
    }


    @Override
    public BaseResponse getCommentListById(Context ctx, String subjectId) {
        BaseResponse baseResponse = CommentBaseListResponseFactory.getBaseResponseFrom(commentDao.getCommentListById(ctx, subjectId));
        return baseResponse;
    }

    @Override
    public BaseResponse getFollowingSubjectListByUserId(Context ctx, String userId) {
        BaseResponse baseResponse = SubjectBaseListResponseFactory.getBaseResponseFrom(commentDao.getFollowingSubjectListByUserId(ctx, userId));
        return baseResponse;
    }

    @Override
    public BaseResponse setNewComment(Context ctx, String userId, String text, String subjectId) {
        BaseResponse baseResponse = BaseResponseFactory.getBaseResponseFrom(commentDao.setNewComment(ctx, userId, text, subjectId));
        return baseResponse;
    }

    @Override
    public BaseResponse setLikeComment(Context ctx, String commentId, String userId) {
        BaseResponse baseResponse = BaseResponseFactory.getBaseResponseFrom(commentDao.setLikeComment(ctx, commentId, userId));
        return baseResponse;
    }
}
