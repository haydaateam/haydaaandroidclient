package com.app.haydaaapp.service;

import android.content.Context;

import com.app.haydaaapp.model.BaseResponse;

/**
 * Created by abdullah sarıkaya on 26.8.2016.
 */
public interface SubjectService {
    public BaseResponse getSubjectListByUserId(Context ctx, String userId);

    public BaseResponse getSubjectById(Context ctx, String subjectId);

    public BaseResponse getPopularSubjectList(Context ctx, String userId, String regId);

    public BaseResponse getNewSubjectList(Context ctx, String userId);

    public BaseResponse getSubjectListByUniversity(Context ctx, String universityName);

    public BaseResponse getSubjectListByUniversityAndDepartment(Context ctx, String universityName, String departmentName);

    public BaseResponse setNewSubject(Context ctx, String userId, String text);

    public BaseResponse getSubjectNtfList(Context ctx, String userId);

}
