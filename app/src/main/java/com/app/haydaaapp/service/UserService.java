package com.app.haydaaapp.service;

import android.content.Context;

import com.app.haydaaapp.model.BaseResponse;

/**
 * Created by metin2 on 26.8.2016.
 */
public interface UserService {

    public BaseResponse getUserById(Context ctx, String userId);

    public BaseResponse getUserByUserNameAndCode(Context ctx, String userName, String code);

    public BaseResponse newUser(Context ctx, String userEmail, String userName, String userUniversity, String userDepartment, String userGender, String userGradDate, String userCode, String deviceKey);


}
