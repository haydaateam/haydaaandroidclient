package com.app.haydaaapp.service.impl;

import android.content.Context;

import com.app.haydaaapp.dao.SubjectDao;
import com.app.haydaaapp.dao.impl.SubjectDaoImpl;
import com.app.haydaaapp.factory.BaseResponseFactory;
import com.app.haydaaapp.factory.SubjectBaseListResponseFactory;
import com.app.haydaaapp.model.BaseResponse;
import com.app.haydaaapp.service.SubjectService;

/**
 * Created by abdullah sarıkaya on 26.8.2016.
 */
public class SubjectServiceImpl implements SubjectService {

    private SubjectDao subjectDao;

    public SubjectServiceImpl() {
        this.subjectDao = new SubjectDaoImpl();
    }

    @Override
    public BaseResponse getSubjectListByUserId(Context ctx, String userId) {
        BaseResponse baseResponse = SubjectBaseListResponseFactory.getBaseResponseFrom(subjectDao.getSubjectListByUserId(ctx, userId));
        return baseResponse;
    }

    @Override
    public BaseResponse getSubjectById(Context ctx, String subjectId) {
        BaseResponse baseResponse = BaseResponseFactory.getBaseResponseFrom(subjectDao.getSubjectById(ctx, subjectId));
        return baseResponse;
    }

    @Override
    public BaseResponse getPopularSubjectList(Context ctx, String userId, String regId) {
        BaseResponse baseResponse = SubjectBaseListResponseFactory.getBaseResponseFrom(subjectDao.getPopularSubjectList(ctx, userId, regId));
        return baseResponse;
    }

    @Override
    public BaseResponse getNewSubjectList(Context ctx, String userId) {
        BaseResponse baseResponse = SubjectBaseListResponseFactory.getBaseResponseFrom(subjectDao.getNewSubjectList(ctx, userId));
        return baseResponse;
    }

    @Override
    public BaseResponse getSubjectListByUniversity(Context ctx, String universityName) {
        BaseResponse baseResponse = SubjectBaseListResponseFactory.getBaseResponseFrom(subjectDao.getSubjectListByUniversity(ctx, universityName));
        return baseResponse;
    }

    @Override
    public BaseResponse getSubjectListByUniversityAndDepartment(Context ctx, String universityName, String departmentName) {
        BaseResponse baseResponse = SubjectBaseListResponseFactory.getBaseResponseFrom(subjectDao.getSubjectListByUniversityAndDepartment(ctx, universityName, departmentName));
        return baseResponse;
    }

    @Override
    public BaseResponse setNewSubject(Context ctx, String userId, String text) {
        BaseResponse baseResponse = BaseResponseFactory.getBaseResponseFrom(subjectDao.setNewSubject(ctx, userId, text));
        return baseResponse;
    }

    @Override
    public BaseResponse getSubjectNtfList(Context ctx, String userId) {
        BaseResponse baseResponse = SubjectBaseListResponseFactory.getBaseResponseFrom(subjectDao.getSubjectNtfList(ctx, userId));
        return baseResponse;
    }
}
