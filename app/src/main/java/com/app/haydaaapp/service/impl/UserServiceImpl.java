package com.app.haydaaapp.service.impl;

import android.content.Context;

import com.app.haydaaapp.dao.UserDao;
import com.app.haydaaapp.dao.impl.UserDaoImpl;
import com.app.haydaaapp.factory.BaseResponseFactory;
import com.app.haydaaapp.model.BaseResponse;
import com.app.haydaaapp.service.UserService;

/**
 * Created by abdullah sarıkaya on 26.8.2016.
 */
public class UserServiceImpl implements UserService {

    private UserDao userDao;

    public UserServiceImpl() {
        this.userDao = new UserDaoImpl();
    }

    @Override
    public BaseResponse getUserById(Context ctx, String userId) {
        BaseResponse baseResponse = BaseResponseFactory.getBaseResponseFrom(userDao.getUserById(ctx, userId));
        return baseResponse;
    }

    @Override
    public BaseResponse getUserByUserNameAndCode(Context ctx, String userName, String code) {
        BaseResponse baseResponse = BaseResponseFactory.getBaseResponseFrom(userDao.getUserByUserNameAndCode(ctx, userName, code));
        return baseResponse;
    }

    @Override
    public BaseResponse newUser(Context ctx, String userEmail, String userName, String userUniversity, String userDepartment, String userGender, String userGradDate, String userCode, String deviceKey) {
        BaseResponse baseResponse = BaseResponseFactory.getBaseResponseFrom(userDao.newUser(ctx, userEmail, userName, userUniversity, userDepartment, userGender, userGradDate, userCode, deviceKey));
        return baseResponse;
    }
}
