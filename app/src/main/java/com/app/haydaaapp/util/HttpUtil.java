package com.app.haydaaapp.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

public class HttpUtil {

    private static final int CONNECTION_TIMEOUT = 10000; // 10 seconds connection timeout
    private static final int SOCKET_TIMEOUT = 120000; // 2 minutes read timeout
    private static CookieManager cookieManager;

    public HttpUtil() {
        CookieManager cookieManager = new CookieManager();
        CookieHandler.setDefault(cookieManager);
    }

    // HTTP POST request
    public static String sendPost(String url, Map<String, String> headers, Map<String, String> commonHeaders)
            throws IOException {
        return getJSON(url, headers, commonHeaders, "POST", null);
    }


    // HTTP POST request
    public static String sendPost(String url, Map<String, String> headers, Map<String, String> commonHeaders, String jsonBody)
            throws IOException {
        return getJSON(url, headers, commonHeaders, "POST", jsonBody);
    }

    // HTTP GET request
    public static String sendGet(String url, Map<String, String> headers, Map<String, String> commonHeaders) throws IOException {
        return getJSON(url, headers, commonHeaders, "GET", null);
    }


    public static String getJSON(String url, Map<String, String> headers, Map<String, String> commonHeaders, String method, String json) {

        // Set cookie manager to keep session
        if (cookieManager == null) {
            cookieManager = new CookieManager();
            CookieHandler.setDefault(cookieManager);
        }

        HttpURLConnection connection = null;
        try {
            URL u = new URL(url);
            connection = (HttpURLConnection) u.openConnection();
            connection.setRequestMethod(method);

            connection.setAllowUserInteraction(false);
            connection.setConnectTimeout(CONNECTION_TIMEOUT);
            connection.setReadTimeout(SOCKET_TIMEOUT);

            if (commonHeaders != null) {
                // add common headers
                for (Map.Entry<String, String> header : commonHeaders.entrySet()) {
                    connection.setRequestProperty(header.getKey(), header.getValue());
                }
            }

            if (headers != null) {
                // add headers
                for (Map.Entry<String, String> header : headers.entrySet()) {
                    connection.setRequestProperty(header.getKey(), header.getValue());
                }
            }

            if (json != null) {
                //set the content length of the body
                connection.setRequestProperty("Content-length", json.getBytes().length + "");
                connection.setDoInput(true);
                connection.setDoOutput(true);
                connection.setUseCaches(false);

                //send the json as body of the request
                OutputStream outputStream = connection.getOutputStream();
                outputStream.write(json.getBytes("UTF-8"));
                outputStream.close();
            }

            int status = connection.getResponseCode();

//            Map<String, List<String>> headerFields = connection.getHeaderFields();
//            List<String> cookiesHeader = headerFields.get("Set-Cookie");
//            if (cookiesHeader != null) {
//                for (String cookie : cookiesHeader) {
//                    cookieManager.getCookieStore().add(null, HttpCookie.parse(cookie).get(0));
//                }
//            }
//

            switch (status) {
                case 200:
                case 201:
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    StringBuilder sb = new StringBuilder();
                    String line;
                    while ((line = bufferedReader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    bufferedReader.close();
//                    Utils.LogD("Received String : " + sb.toString());
                    return sb.toString();
            }

        } catch (MalformedURLException ex) {
            Utils.LogE("HTTP Client", "Error in http connection" + ex.toString());
        } catch (IOException ex) {
            Utils.LogE("HTTP Client", "Error in http connection" + ex.toString());
        } catch (Exception ex) {
            Utils.LogE("HTTP Client", "Error in http connection" + ex.toString());
        } finally {
            if (connection != null) {
                try {
                    connection.disconnect();
                } catch (Exception ex) {
                    Utils.LogE("HTTP Client", "Error in http connection" + ex.toString());
                }
            }
        }
        return null;
    }
}
