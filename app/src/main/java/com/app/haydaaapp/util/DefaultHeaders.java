package com.app.haydaaapp.util;

import android.content.Context;
import android.os.Build;

import com.app.haydaaapp.R;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Adk on 30/06/16.
 */
public class DefaultHeaders {

    Context context;

    public DefaultHeaders(Context context) {
        this.context = context;
    }

    public Map<String, String> getDefaultHeaders() {
        // add default headers

        Map<String, String> headers = new HashMap<String, String>();

        headers.put("Content-type", "application/json");
        headers.put("Accept", "application/json");
        headers.put("Accept-Charset", "UTF-8");
        headers.put("installationKey", "");
        headers.put("applicationId", context.getString(R.string.app_name));
        headers.put("authKey", "1234");
        headers.put("userId", "xx");
        headers.put("deviceBrand", Build.BRAND);
        headers.put("deviceModel", Build.MODEL);
        headers.put("platformVersion", Build.VERSION.RELEASE);

        return headers;
    }
}
