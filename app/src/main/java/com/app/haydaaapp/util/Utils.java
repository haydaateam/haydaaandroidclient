package com.app.haydaaapp.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;

import com.app.haydaaapp.BuildConfig;
import com.app.haydaaapp.R;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by Adk on 03/07/16.
 */
public class Utils {

    public static final boolean DEBUG = BuildConfig.DEBUG;
    public static final String LOG_TAG = "HAYDAA";

    /**
     * To log with debug message
     */
    public static void LogD(String message) {
        if (DEBUG)
            Log.d(LOG_TAG, message);
    }

    /**
     * To log with error message
     */
    public static void LogE(String log, String message) {
        Log.e(log, message);
    }

    /**
     * To log with info message
     */
    public static void LogI(String log, String message) {
        if (DEBUG)
            Log.i(log, message);
    }

    /**
     * To log with warn message
     */
    public static void LogW(String log, String message) {
        if (DEBUG)
            Log.w(log, message);
    }

    /**
     * To log with verbose message
     */
    public static void LogV(String log, String message) {
        if (DEBUG)
            Log.v(log, message);
    }

    public static String convertEncodeCharacters(String text) {
        text = text.replace("ı", "\\u0131");
        text = text.replace("ş", "\\u015F");
        text = text.replace("ğ", "\\u011F");
        text = text.replace("İ", "\\u0130");
        text = text.replace(" ", "%20");
        text = text.replace("ü", "%FC");
        text = text.replace("Ü", "%DC");
        text = text.replace("ö", "%F6");
        text = text.replace("Ö", "%D6");
        text = text.replace("ç", "%E7");
        text = text.replace("Ç", "%C7");
        return text;
    }

    public static boolean checkForUpdates() {

        return false;
    }

    public static String convertDecodeCharacters(String text) {

        text = text.replace("\\u0131", "ı");
        text = text.replace("\\u015F", "ş");
        text = text.replace("\\u011F", "ğ");
        text = text.replace("\\u0130", "İ");
        text = text.replace("%FC", "ü");
        text = text.replace("%DC", "Ü");
        text = text.replace("%20", " ");
        text = text.replace("%F6", "ö");
        text = text.replace("%D6", "Ö");
        text = text.replace("%E7", "ç");
        text = text.replace("%C7", "Ç");
        return text;
    }


    public final static boolean isValidEmail(CharSequence target) {
        if (TextUtils.isEmpty(target)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    public static ProgressDialog showProgress(Context context, String title, String message) {
        ProgressDialog progress = ProgressDialog.show(context, title,
                message, true, false);
        return progress;
    }

    /**
     * @category View utils
     */
    public static void showAlertDialog(Context context, String title,
                                       String message) {
        final AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        if (title == null) {
            alertDialog.setMessage("\n" + message);
        } else {
            alertDialog.setTitle(title);
            alertDialog.setMessage(message);
        }
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE,
                context.getString(R.string.dialogButtonOk),
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        alertDialog.dismiss();
                    }
                });

        alertDialog.show();
    }

    /**
     * To close keyboard
     */
    public static void CloseKeyboard(Activity act) {
        InputMethodManager imm = (InputMethodManager) act
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        View focused = act.getCurrentFocus();
        if (focused != null)
            imm.hideSoftInputFromWindow(focused.getWindowToken(), 0);
    }

    public static boolean isInternetConnection(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }


    public static <T> void notifyDataSetChanged(ArrayAdapter<T> arrayAdapter) {
        if (arrayAdapter != null) {
            arrayAdapter.notifyDataSetChanged();
        }
    }

    public static String getMd5(String str) {

        String original = str;
        MessageDigest md;
        StringBuffer sb = null;
        byte[] digest = null;
        try {
            md = MessageDigest.getInstance("MD5");
            md.update(original.getBytes());
            digest = md.digest();
            sb = new StringBuffer();
            for (byte b : digest) {
                sb.append(Integer.toHexString((int) (b & 0xff)));
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }

    public static String mixMD5Strings(String s1, String s2) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < s1.length(); i++) {
            sb.append(s1.charAt(i));
            sb.append(s2.charAt(i));
        }

        return sb.toString().substring(0, 32);
    }

    public static Bitmap getCircleBitmap(Bitmap bitmap) {
        final Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        final Canvas canvas = new Canvas(output);

        final int color = Color.RED;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawOval(rectF, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        bitmap.recycle();

        return output;
    }

}
