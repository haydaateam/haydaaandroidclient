package com.app.haydaaapp.util;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Adk on 04/07/16.
 */
public class AppSharedPrefs {

    private static AppSharedPrefs instance = null;


    private static final String USER_ID = "UserId";
    private static final String SHOW_TUTORIAL = "ShowTutorial";
    private static final String DEVICE_TOKEN = "deviceToken";


    private SharedPreferences mSharedPrefs;

    public static AppSharedPrefs getInstance(Context context) {
        if (instance == null) {
            instance = new AppSharedPrefs(context);
        }
        return instance;
    }

    private AppSharedPrefs(Context context) {
        mSharedPrefs = context.getSharedPreferences("generalPrefs", context.MODE_PRIVATE);
    }


    public String getUserId() {
        return mSharedPrefs.getString(USER_ID, null);
    }

    public void setUserId(String userId) {
        SharedPreferences.Editor editor = mSharedPrefs.edit();
        editor.putString(USER_ID, userId);
        editor.commit();
    }

    public String getDeviceToken() {


        return mSharedPrefs.getString(DEVICE_TOKEN, "");

    }

    public void setDeviceToken(String deviceToken) {
        SharedPreferences.Editor editor = mSharedPrefs.edit();
        editor.putString(DEVICE_TOKEN, deviceToken);
        editor.commit();
    }

    public boolean getShowTutorial() {
        return mSharedPrefs.getBoolean(SHOW_TUTORIAL, false);
    }

    public void setShowTutorial(boolean showTutorial) {
        SharedPreferences.Editor editor = mSharedPrefs.edit();
        editor.putBoolean(SHOW_TUTORIAL, showTutorial);
        editor.commit();
    }

    public void clearUser() {
        mSharedPrefs.edit().putString(USER_ID, null).commit();
    }


}
