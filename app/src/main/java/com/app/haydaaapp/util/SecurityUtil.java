package com.app.haydaaapp.util;

import android.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class SecurityUtil {

    private static SecurityUtil instance = null;

    private static final String keyValue = "1357642812341234";

    private byte[] key;

    public static synchronized SecurityUtil getInstance() {
        if (instance == null) {
            instance = new SecurityUtil();
        }
        return instance;

    }

    private SecurityUtil() {
        this.key = keyValue.getBytes();

    }

    public String decryptAES(String toDecrypt) throws Exception {

        SecretKeySpec secretKeySpec = new SecretKeySpec(key, "AES");

        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.DECRYPT_MODE, secretKeySpec);

        byte[] decrpted = Base64.decode(toDecrypt, Base64.DEFAULT);
        byte[] decryptedValue = cipher.doFinal(decrpted);

        return new String(decryptedValue, "UTF-8");
    }


    public String encryptedAES(String toEncrypt) throws Exception {

        SecretKeySpec secretKeySpec = new SecretKeySpec(key, "AES");

        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec);

        byte[] encrypted = cipher.doFinal(toEncrypt.getBytes("UTF-8"));
        String encryptedValue = Base64.encodeToString(encrypted, Base64.DEFAULT);

        return encryptedValue;
    }


}
