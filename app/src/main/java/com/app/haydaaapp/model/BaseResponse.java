package com.app.haydaaapp.model;

/**
 * Created by Adk on 30/06/16.
 */
public class BaseResponse extends BaseModel {

    private String code;
    private String message;
    private Object responseObject;
    private String responseObjectType;

    public String getResponseObjectType() {
        return responseObjectType;
    }

    public void setResponseObjectType(String responseObjectType) {
        this.responseObjectType = responseObjectType;
    }

    public Object getResponseObject() {
        return responseObject;
    }

    public void setResponseObject(Object responseObject) {
        this.responseObject = responseObject;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }


}
