package com.app.haydaaapp.dao.impl;

import android.content.Context;

import com.app.haydaaapp.R;
import com.app.haydaaapp.dao.SubjectDao;
import com.app.haydaaapp.util.HttpUtil;
import com.app.haydaaapp.util.Utils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by abdullah sarıkaya on 25.8.2016.
 */
public class SubjectDaoImpl implements SubjectDao {
    @Override
    public String getSubjectListByUserId(Context ctx, String userId) {
        String response = null;
        try {
            response = HttpUtil.sendGet(ctx.getResources().getString(R.string.subjectListByUserIdUrl) + "?userId=" + userId, null, null);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    @Override
    public String getSubjectById(Context ctx, String subjectId) {
        String response = null;
        try {
            response = HttpUtil.sendGet(ctx.getResources().getString(R.string.subjectByIdUrl) + "?subjectId=" + subjectId, null, null);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    @Override
    public String getPopularSubjectList(Context ctx, String userId, String regId) {
        String response = null;
        try {
            response = HttpUtil.sendGet(ctx.getResources().getString(R.string.subjectPopularListUrl) + "?userId=" + userId + "&regId=" + regId, null, null);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    @Override
    public String getNewSubjectList(Context ctx, String userId) {
        String response = null;
        try {
            response = HttpUtil.sendGet(ctx.getResources().getString(R.string.allSubjectListUrl) + "?userId=" + userId, null, null);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    @Override
    public String getSubjectListByUniversity(Context ctx, String universityName) {
        String response = null;
        try {
            response = HttpUtil.sendGet(ctx.getResources().getString(R.string.allSubjectListByUniversityUrl) + "?universityName=" + universityName, null, null);
            Utils.LogD(ctx.getResources().getString(R.string.allSubjectListByUniversityUrl) + "?universityName=" + universityName);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    @Override
    public String getSubjectListByUniversityAndDepartment(Context ctx, String universityName, String departmentName) {
        String response = null;
        try {
            response = HttpUtil.sendGet(ctx.getResources().getString(R.string.allSubjectListUniversityAndDepartmentUrl) + "?universityName=" +
                    universityName + "&departmentName=" + departmentName, null, null);
            Utils.LogD(ctx.getResources().getString(R.string.allSubjectListUniversityAndDepartmentUrl) + "?universityName=" +
                    universityName + "&departmentName=" + departmentName);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    @Override
    public String setNewSubject(Context ctx, String userId, String text) {
        String response = null;
        Map<String, String> map = new HashMap<>();
        map.put("user-id", userId);
        map.put("text", text);
        try {
            response = HttpUtil.sendPost(ctx.getResources().getString(R.string.newSubjectUrl), map, null);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    @Override
    public String getSubjectNtfList(Context ctx, String userId) {
        String response = null;
        try {
            response = HttpUtil.sendGet(ctx.getResources().getString(R.string.ntfUrl) + "?userId=" + userId, null, null);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }
}
