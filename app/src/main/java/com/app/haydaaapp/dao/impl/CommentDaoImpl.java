package com.app.haydaaapp.dao.impl;

import android.content.Context;

import com.app.haydaaapp.R;
import com.app.haydaaapp.dao.CommentDao;
import com.app.haydaaapp.util.HttpUtil;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by abdullah sarıkaya on 25.8.2016.
 */
public class CommentDaoImpl implements CommentDao {
    @Override
    public String getCommentListById(Context ctx, String subjectId) {
        String response = null;
        try {
            response = HttpUtil.sendGet(ctx.getResources().getString(R.string.commentListBySubjectIdUrl) + "?subjectId=" + subjectId, null, null);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    @Override
    public String getFollowingSubjectListByUserId(Context ctx, String userId) {
        String response = null;
        try {
            response = HttpUtil.sendGet(ctx.getResources().getString(R.string.commentSubjectListByUserUrl) + "?userId=" + userId, null, null);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    @Override
    public String setNewComment(Context ctx, String userId, String text, String subjectId) {
        String response = null;

        Map<String, String> map = new HashMap<>();
        map.put("user-id", userId);
        map.put("text", text);
        map.put("subject-id", subjectId);
        try {
            response = HttpUtil.sendPost(ctx.getResources().getString(R.string.newCommentUrl), map, null);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    @Override
    public String setLikeComment(Context ctx, String commentId, String userId) {
        String response = null;
        Map<String, String> map = new HashMap<>();
        map.put("comment-id", commentId);
        map.put("user-id", userId);
        try {
            response = HttpUtil.sendPost(ctx.getResources().getString(R.string.setLikeUrl), map, null);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }
}
