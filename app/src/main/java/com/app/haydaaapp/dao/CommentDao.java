package com.app.haydaaapp.dao;

import android.content.Context;

/**
 * Created by abdullah sarıkaya on 23.8.2016.
 */
public interface CommentDao {

    public String getCommentListById(Context ctx, String subjectId);

    public String getFollowingSubjectListByUserId(Context ctx, String subjectId);

    public String setNewComment(Context ctx, String userId, String text, String subjectId);

    public String setLikeComment(Context ctx, String commentId, String userId);

}
