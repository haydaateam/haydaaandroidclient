package com.app.haydaaapp.dao;

import android.content.Context;

/**
 * Created by abdullah sarıkaya on 29.8.2016.
 */
public interface UniversityAndDepartmentDao {

    public String getUniversityList(Context ctx);

    public String getDepartmentList(Context ctx);
}
