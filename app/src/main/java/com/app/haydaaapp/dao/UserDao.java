package com.app.haydaaapp.dao;

import android.content.Context;

/**
 * Created by abdullah sarıkaya on 23.8.2016.
 */
public interface UserDao {

    public String getUserById(Context ctx, String userId);

    public String getUserByUserNameAndCode(Context ctx, String userName, String code);

    public String newUser(Context ctx, String userEmail, String userName, String userUniversity, String userDepartment, String userGender, String userGradDate, String userCode, String regId);

}
