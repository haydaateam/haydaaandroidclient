package com.app.haydaaapp.dao.impl;

import android.content.Context;

import com.app.haydaaapp.R;
import com.app.haydaaapp.dao.UserDao;
import com.app.haydaaapp.util.HttpUtil;
import com.app.haydaaapp.util.SecurityUtil;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by abdullah sarıkaya on 25.8.2016.
 */
public class UserDaoImpl implements UserDao {
    @Override
    public String getUserById(Context ctx, String userId) {
        String response = null;
        try {
            response = HttpUtil.sendGet(ctx.getResources().getString(R.string.UserInfoByIdUrl) + "?userId=" + userId, null, null);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    @Override
    public String getUserByUserNameAndCode(Context ctx, String userName, String code) {
        String response = null;

        try {
            response = HttpUtil.sendGet(ctx.getResources().getString(R.string.UserInfoByNameCodeUrl) + "?name="
                    + userName + "&code=" + code, null, null);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    @Override
    public String newUser(Context ctx, String userEmail, String userName, String userUniversity, String userDepartment, String userGender, String userGradDate, String userCode, String regId) {
        String response = null;
        Map<String, String> map = new HashMap<>();
        map.put("email", userEmail);
        map.put("userName", userName);
        map.put("userUniversity", userUniversity);
        map.put("userDepartment", userDepartment);
        map.put("userGender", userGender);
        map.put("userGradDate", userGradDate);

        try {
            map.put("userCode", SecurityUtil.getInstance().encryptedAES(userCode));
            map.put("regId", regId);
            response = HttpUtil.sendPost(ctx.getResources().getString(R.string.newUserUrl), map, null);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return response;
    }
}
