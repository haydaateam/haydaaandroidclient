package com.app.haydaaapp.dao;

import android.content.Context;

/**
 * Created by abdullah sarıkaya on 23.8.2016.
 */
public interface SubjectDao {

    public String getSubjectListByUserId(Context ctx, String userId);

    public String getSubjectById(Context ctx, String subjectId);

    public String getPopularSubjectList(Context ctx, String userId, String regId);

    public String getNewSubjectList(Context ctx, String userId);

    public String getSubjectListByUniversity(Context ctx, String universityName);

    public String getSubjectListByUniversityAndDepartment(Context ctx, String universityName, String departmentName);

    public String setNewSubject(Context ctx, String userId, String text);

    public String getSubjectNtfList(Context ctx, String userId);

}
