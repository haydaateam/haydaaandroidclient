package com.app.haydaaapp.dao.impl;

import android.content.Context;

import com.app.haydaaapp.R;
import com.app.haydaaapp.dao.UniversityAndDepartmentDao;
import com.app.haydaaapp.util.HttpUtil;

import java.io.IOException;

/**
 * Created by abdullah sarıkaya on 29.8.2016.
 */
public class UniversityAndDepartmentDaoImpl implements UniversityAndDepartmentDao {
    @Override
    public String getUniversityList(Context ctx) {
        String response = null;

        try {
            response = HttpUtil.sendGet(ctx.getResources().getString(R.string.universityListUrl), null, null);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    @Override
    public String getDepartmentList(Context ctx) {
        String response = null;

        try {
            response = HttpUtil.sendGet(ctx.getResources().getString(R.string.departmentListUrl), null, null);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }
}
