package com.app.haydaaapp.gcm;

import android.app.IntentService;
import android.content.Intent;

import com.app.haydaaapp.R;
import com.app.haydaaapp.util.AppSharedPrefs;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import java.io.IOException;

/**
 * Created by Abdullah on 25.9.2016.
 */
public class RegistrationIntentService extends IntentService {

    private static final String TAG = "RegIntentService";


    public RegistrationIntentService() {
        super(TAG);
    }

    @Override
    public void onHandleIntent(Intent intent) {


        InstanceID instanceID = InstanceID.getInstance(this);
        try {
            String token = instanceID.getToken(getResources().getString(R.string.gcm_defaultSenderId),
                    GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);

            AppSharedPrefs.getInstance(RegistrationIntentService.this).setDeviceToken(token);

        } catch (IOException e) {
            e.printStackTrace();
        }


        // ...
    }

    // ...
}
