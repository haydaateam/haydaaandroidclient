package com.app.circleppgmailandroidlib;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by abdullah sarıkaya on 26.8.2016.
 */
public class CircleImageView extends View {

    private Paint paint;
    private String gender;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    private String userName;

    public CircleImageView(Context context) {
        super(context);
        paint = new Paint();

    }

    public CircleImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int x = getWidth();
        int y = getHeight();

        int radius;
        radius = 50;

        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.TRANSPARENT);
        canvas.drawPaint(paint);

        if (gender == null || userName == null) {
            gender = "Anonim";
            userName = "XXX";
        }
        if (gender.charAt(0) == 'K') {
            paint.setColor(Color.parseColor("#e74c3c"));
        } else if (gender.charAt(0) == 'E') {
            paint.setColor(Color.parseColor("#3498db"));
        } else {
            paint.setColor(Color.parseColor("#00eab5"));
        }


        canvas.drawCircle(x / 2, y / 2, radius, paint);
        Paint textPaint = new Paint();
        textPaint.setColor(Color.WHITE);
        textPaint.setTextSize(45);
        canvas.drawText(userName.substring(0, 1).toUpperCase(), (x / 2) - (radius / 4) - 1, (y / 2) + (radius / 3), textPaint);
    }


}
